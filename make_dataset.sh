#!/bin/bash

builder=test_builder.json

pipenv run python -m src.Data.make_dataset init-builder \
       --builder-state-data=$builder

pipenv run python -m src.Data.make_dataset retrieve-raw-data \
       --builder-state-data=$builder

pipenv run python -m src.Data.make_dataset load-dsa-sqldump \
       --builder-state-data=$builder

pipenv run python -m src.Data.make_dataset migrate-dsa-database \
        --builder-state-data=$builder

pipenv run python -m src.Data.make_dataset extract-pdf-text \
       --builder-state-data=$builder

pipenv run python -m src.Data.make_dataset match-entities \
       --builder-state-data=$builder
