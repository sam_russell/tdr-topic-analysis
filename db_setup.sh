#!/bin/bash
# Creates mysql user and database

u="tdr_topic_analysis_admin"
read -s -p "Create password for tdr_topic_analysis_admin: " pw
dbname="tdr_topic_analysis"
admin_user="root"

validated=echo $p | grep -E "^[a-zA-Z0-9]+$"


chromedriver="chromedriver.bin"

if [ $1 == "gnu/linux" ]
then
    chromedriver="chromedriver"
fi

if [ -n $2 ]
then
    if [ $2 == "-u" ]
    then
	admin_user=$3
    fi
fi


function env_exists () {
    touch .env
    env_wc=$(wc -l .env)
    env_len=${env_wc:0:1}
    echo $env_len
    if [ $env_len == 0 ]
    then
	return 0
    else
	return 1
    fi
}

if [ $(env_exists) != 0 ]
then
    echo "Existing .env file present, no action taken."
    echo "To proceed, please rename .env file as backup and re-run this script."
else
    if [ -n validated ]
    then
	if [ $admin_user == "root" ]
	then
	    sudo mysql --user=$admin_user --execute="CREATE USER '${u}'@'localhost' IDENTIFIED BY '${pw}';"
	else
	    mysql --user=$admin_user --execute="CREATE USER '${u}'@'localhost' IDENTIFIED BY '${pw}';"
	fi
	# mysql --user=root --execute="CREATE DATABASE ${dbname};"
	# mysql --user=root --execute="GRANT ALL PRIVILEGES ON ${dbname} . * TO '${u}'@'localhost';"
	{
	    echo "RAW_DATA_DOWNLOADS_DIR=data/raw/downloads"
	    echo "CHROME_EXEC_PATH=lib/${chromedriver}"
	    echo "MYSQL_USERNAME=${u}"
	    echo "MYSQL_PASSWORD=${pw}"
	    echo "MYSQL_DB_NAME=${dbname}"
	} > .env
    else
	echo "Password must be alphanumeric only, please."
    fi
fi
