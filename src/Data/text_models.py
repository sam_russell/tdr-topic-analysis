from uuid import uuid4
from sqlalchemy import Column, Integer, String, ForeignKey, \
    DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


def uuid_str():
    return str(uuid4())


class Dataset(Base):
    """Metadata for the project and scripts used, etc"""

    __tablename__ = "dataset"

    id = Column(Integer, primary_key=True)
    vc_commit = Column(String)
    created = Column(DateTime)
    modified = Column(DateTime)


class GuidelinePackage(Base):
    """The types of trustworthy data repository certifications"""
    __tablename__ = "guideline_package"

    id = Column(Integer, primary_key=True)
    dsa_db_id = Column(Integer)  # from dsa_db
    name = Column(String)  # from dsa_db
    creation_date = Column(DateTime)  # from dsa_db
    user_manual = Column(String)  # from dsa_db
    created = Column(DateTime)
    modified = Column(DateTime)


class Guideline(Base):
    """An assessment guideline implementd by a certification."""

    __tablename__ = "guideline"

    id = Column(Integer, primary_key=True)
    dsa_db_id = Column(Integer)
    dsa_db_order_num = Column(Integer)
    title = Column(String)  # from dsa_db
    applicant_manual = Column(String)  # from dsa_db
    reviewer_manual = Column(String)  # from dsa_db
    creation_date = Column(DateTime)  # from dsa_db
    created = Column(DateTime)
    modified = Column(DateTime)

    dsa_db_guidelinepackage_id = Column(Integer)
    guideline_package_id = Column(
        Integer, ForeignKey("guideline_package.id"))
    guideline_package = relationship("GuidelinePackage")


class Repository(Base):
    """Contact and contextual information for a data repository"""
    __tablename__ = "repository"

    id = Column(Integer, primary_key=True)
    dsa_db_id = Column(Integer, nullable=True)
    name = Column(String)
    website = Column(String)
    created = Column(DateTime)
    modified = Column(DateTime)


class Assessment(Base):
    """A full assessment report document."""
    __tablename__ = "assessment"

    id = Column(Integer, primary_key=True)
    dsa_db_id = Column(Integer, nullable=True)
    creation_date = Column(DateTime)  # from dsa_db
    seal_date = Column(DateTime)  # from dsa_db
    created = Column(DateTime)
    modified = Column(DateTime)

    dsa_db_guidelinepackage_id = Column(Integer)
    guideline_package_id = Column(
        Integer, ForeignKey("guideline_package.id"))
    guideline_package = relationship(
        "GuidelinePackage")

    dsa_db_repository_id = Column(Integer)
    repository_id = Column(
        Integer, ForeignKey("repository.id"))
    repository = relationship(
        "Repository")
    repository_uuid = Column(Integer)


class AssessmentEntry(Base):
    """Represents a response and score to one of the 16 guidelines sections."""
    __tablename__ = "assessment_entry"

    id = Column(Integer, primary_key=True)
    dsa_db_id = Column(Integer)
    compliance_level = Column(Integer)
    response = Column(String)
    created = Column(DateTime)
    modified = Column(DateTime)

    dsa_db_assessment_id = Column(Integer)
    assessment_id = Column(Integer,
                           ForeignKey('assessment.id'))
    assessment = relationship(
        "Assessment")

    dsa_db_guideline_id = Column(Integer)
    guideline_id = Column(Integer,
                          ForeignKey('guideline.id'))
    guideline = relationship(
        "Guideline")
