from datetime import datetime
import os
import csv


def assessments_count(session, directory_path):
    query = """
SELECT repository.id, repository.name, repository.website, 
COUNT(assessment.id) AS assessments_count
FROM repository
INNER JOIN assessment
ON assessment.repository_id=repository.id
GROUP BY repository.id
ORDER BY assessments_count DESC;
"""
    data = session.execute(query)
    data = data.fetchall()

    filepath = os.path.join(directory_path,
                            'assessments_count_by_repository.csv')
    with open(filepath, 'w', newline='') as csvfile:
        fieldnames = ['repository_id', 'name', 'website', 'assessments_count']
        writer = csv.DictWriter(
            csvfile, fieldnames=fieldnames, quoting=csv.QUOTE_NONNUMERIC)
        writer.writeheader()
        for row in data:
            writer.writerow({
                'repository_id': row.id,
                'name': row.name,
                'website': row.website,
                'assessments_count': row.assessments_count
                })
    return filepath
