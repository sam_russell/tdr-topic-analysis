from datetime import datetime
import functools
import logging
import os
import re
import subprocess
import types
from typing import Callable

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.engine import reflection
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, aliased

from src.Data.database_config import DatabaseConfig
import src.Data.text_models as models

logger = logging.getLogger(__name__)


class MySQLDataLoader:
    """Creates and populates MySQL db"""

    def __init__(self,
                 db_name=None,
                 user=None,
                 password=None,
                 db_host=None,
                 db_port=None,
                 log_file_handler: logging.FileHandler = None,
                 sqldump_filepath: str = None,
                 **kwargs):
        self.db_name = db_name
        self.user = user
        self.password = password
        self.host = db_host
        self.port = db_port
        self.sqldump_filepath = sqldump_filepath
        self.log_file_handler = log_file_handler
        if log_file_handler:
            logger.addHandler(log_file_handler)
    
    @classmethod
    def from_db_config(cls, db_config):
        logger.info("Creating MySQLLoader from DatabaseConfig")
        return cls(**{k: v for k, v
                      in db_config.__dict__ if k[0] != "_"})

    @classmethod
    def from_env(cls):
        logger.info("Creating MySQLLoader from env vars")
        return cls(**{
            "db_name": os.environ.get("MYSQL_DB_NAME"),
            "user": os.environ.get("MYSQL_USERNAME"),
            "password": os.environ.get("MYSQL_PASSWORD"),
            "host": os.environ.get("MYSQL_DB_HOST"),
            "port": os.environ.get("MYSQL_DB_PORT")
        })

    def set_log_file_handler(self, file_handler: logging.FileHandler):
        self.log_file_handler = file_handler
        logger.addHandler(file_handler)

    def make_args(self, **kwargs):
        kwargs = {
            **{"host": self.host, "port": self.port},
            **kwargs
        }
        args = [
            "mysql",
            f"--user={self.user}",
            f"--password={self.password}",
        ]
        for k, v in kwargs.items():
            if v:
                args.append(f"--{k}={v}")
        return args

    def check_db_exists(self):
        check_db_args = self.make_args(database=self.db_name)
        check_db = subprocess.Popen(
            check_db_args,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE
        )
        # TODO: appears to block indefinitely here if there actually
        # is an existing database?
        return check_db.communicate()

    def create_db(self):
        logging.info(
            f"{self.user} attempting to create database {self.db_name}")
        create_db_args = self.make_args(
            execute=f"CREATE DATABASE {self.db_name};")
        create_db = subprocess.Popen(
            create_db_args,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE
        )
        return create_db.communicate()

    def load_sql(self, sqldump_file_path):
        logger.info(
            " ".join([
                f"{self.user} attempting to load {sqldump_file_path} to",
                f"database {self.db_name}"
            ])
        )
        with open(sqldump_file_path) as f:
            load = subprocess.run(
                self.make_args(database=self.db_name),
                stdin=f,
                stderr=subprocess.STDOUT,
                stdout=subprocess.PIPE
            )
            return load

    def make_database(self, sqldump_file_path: str = None,
                      db_exists: bool = False):
        sqldump_file_path = sqldump_file_path or self.sqldump_filepath
        if not db_exists:
            try:
                db_exists = self.check_db_exists()
                if re.search(r"ERROR 1049", db_exists[0].decode()):
                    # this means no DB by this name exists, we can create it.
                    created = self.create_db()
                    logging.info(f"{created}")
                    data = self.load_sql(sqldump_file_path)
                    logging.info(f"{data}")
                else:
                    print(db_exists)
                    raise ValueError("Script bungled")
            except Exception as e:
                logging.error(
                    " ".join([
                        "Unhandled exception while",
                        f"creating database {self.db_name}"
                    ]),
                    exc_info=True)
                raise e
        else:
            logger.info("Using existing database: {self.db_name}")
            data = self.load_sql(sqldump_file_path)
            logger.info(f"{data}")


class Tables:

    def __init__(self):
        pass

    def __getitem__(self, item):
        return self.__dict__[item]


class MySQLManager:

    def __init__(self,
                 driver="mysql+pymysql://",
                 user=None,
                 password=None,
                 db_name=None,
                 db_host=None,
                 db_port=None,
                 **kwargs):
        self.driver = driver
        self.user = user
        self.password = password
        self.db_name = db_name
        self.db_host = db_host
        self.db_port = db_port
        self.engine = None
        self.session = None
        self.meta = None
        self.Base = None
        self.tables = Tables()

    def make_engine(self):
        connect_string = "".join([
            f"{self.driver}{self.user}:{self.password}",
            f"@{self.db_host}:{self.db_port}/{self.db_name}"
        ])
        return create_engine(connect_string)

    def load_meta(self, engine):
        return MetaData(bind=engine, reflect=True)

    def make_tables(self):
        for tbl in self.meta.tables.values():
            t = type(tbl.name,
                     (self.Base,),
                     {"__table__": tbl})
            self.tables.__dict__[tbl.name] = t

    @classmethod
    def make(cls, **kwargs):
        inst = cls(**kwargs)
        inst.engine = inst.make_engine()
        inst.meta = inst.load_meta(inst.engine)
        inst.Base = declarative_base(metadata=inst.meta)
        inst.make_tables()
        Session = sessionmaker(bind=inst.engine)
        inst.session = Session()
        return inst

    @classmethod
    def from_db_config(cls, db_config: DatabaseConfig):
        return cls.make(**db_config.as_dict())


class SQLiteManager:

    def __init__(self, db_filepath=None):
        self.session = None
        self.engine = None
        self.db_filepath = db_filepath
        self.base = models.Base

    def make_engine(self):
        return create_engine(f"sqlite:///{self.db_filepath}")

    def make_tables(self):
        self.base.metadata.create_all(self.engine)

    @classmethod
    def make(cls, db_filepath=None):
        if not db_filepath:
            created = datetime.now().strftime('%FT%T')
            db_filename = f"project_data_{created}.db"
            db_filepath = os.path.join(
                os.environ.get("INTERIM_DATA_DIR"),
                db_filename
                )
            print(db_filepath)
        inst = cls(db_filepath)
        logger.info(
            f"Creating Migration instance for database at {db_filepath}")
        inst.engine = inst.make_engine()
        Session = sessionmaker(bind=inst.engine)
        inst.session = Session()
        return inst

    @classmethod
    def create(cls):
        """Creates a new database"""
        inst = cls.make()
        inst.make_tables()
        return inst

    @classmethod
    def from_env(cls):
        fpath = os.environ.get("SQLITE_DB_FILEPATH")
        inst = cls.make(db_filepath=fpath)
        inst.make_tables()
        return inst

    @classmethod
    def connect(cls, db_filepath, **kwargs):
        """Connects to an existing database"""
        return cls.make(db_filepath=db_filepath, **kwargs)


class MigrationStep:
    """A command object encasulating a step in migration process"""
    def __init__(self,
                 log_file_handler = None,
                 session = None, *args, **kwargs):
        self.log_file_handler = log_file_handler
        self.session = session
        self.func_name = None
        self.func = None

    def execute(self, *args, **kwargs):
        print(self.func_name)
        return self.func(session=self.session, *args, **kwargs)


class Migration:
    """Defines, collects, and executes MigrationStep commands"""
    def __init__(self,
                 mysql_manager=None,
                 sqlite_manager=None,
                 entity_matcher=None):
        self._mysql = mysql_manager
        self._sqlite = sqlite_manager
        self.steps = []
        self.completed = []

    def get_tables(self):
        try:
            return self._mysql.tables
        except Exception as e:
            print(e)
            return None

    def mysql(self, *args, **kwargs):
        def mysql_d(func, *args, **kwargs):
            def inner():
                step = MigrationStep(session=self._mysql.session)
                step.func = func
                step.func_name = func.__name__
                return functools.partial(step.execute, *args, **kwargs)
            return inner
        return functools.partial(mysql_d, *args, **kwargs)

    def sqlite(self, *args, **kwargs):
        def sqlite_d(func, *args, **kwargs):
            def inner():
                step = MigrationStep(session=self._sqlite.session)
                step.func = func
                step.func_name = func.__name__
                return functools.partial(step.execute, *args, **kwargs)
            return inner
        return functools.partial(sqlite_d, *args, **kwargs)

    def add_step(self, decorated_step: Callable):
        cmd = decorated_step()
        self.steps.append(cmd)

    def add_steps(self, *steps):
        step_list = [i for i in steps]
        step_list.reverse()
        for step in step_list:
            self.add_step(step)

    def run(self):
        pipe_kwargs = {}
        while self.steps:
            try:
                cmd = self.steps.pop()
                pipe_kwargs = cmd(**pipe_kwargs) or {}
                self.completed.append(cmd)
            except Exception as e:
                print(e)
                return cmd
        print("all done")


class MigrationFactory:

    @staticmethod
    def make_migration(db_config = None,
                       sqlite_db_filepath = None,
                       existing_sqlite_db = False,
                       test=False,
                       **kwargs):
        if existing_sqlite_db:
            sqlite_mgr = SQLiteManager.make(sqlite_db_filepath)
        else:
            sqlite_mgr = SQLiteManager.from_env()
        if db_config:
            mysql_mgr = MySQLManager.from_db_config(db_config)
        else:
            mysql_mgr = None
        return Migration(mysql_mgr, sqlite_mgr)


def test_migration(db_config):
    mysql_mgr = MySQLManager.from_db_config(db_config)
    migration = Migration(mysql_mgr)

    @migration.mysql(pet_name="soda")
    def say_hi(session, pet_name):
        print(session)
        print(pet_name)
        print("hi")

    say_hi()
    return say_hi


def test_migration2(db_config):
    mysql_mgr = MySQLManager.from_db_config(db_config)
    migration = Migration(mysql_mgr)

    @migration.mysql()
    def query_repositories(session):
        return session.query(
            mysql_mgr.tables.contact_data_repository
        ).first()

    return query_repositories()

def test_migration3(db_config):
    mysql_mgr = MySQLManager.from_db_config(db_config)
    migration = Migration(mysql_mgr)

    @Migration.mysql(extra="neat")
    def query_repositories(session, extra):
        print(extra)
        return session.query(
            mysql_mgr.tables.contact_data_repository
        ).first()

    return query_repositories(migration)


def test_migration4(db_config, stuff):
    mysql_mgr = MySQLManager.from_db_config(db_config)
    migration = Migration(mysql_mgr)

    @migration.mysql(stuff)
    def do_stuff(session, stuff):
        print(session)
        print(stuff)
        print("cool")

    return do_stuff
