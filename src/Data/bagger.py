from datetime import datetime
import os
from typing import Dict

import bagit


class Bagger:
    """Uses LoC Bagit to assure raw data provenance and fixity"""
    class BagException(Exception):
        pass

    def __init__(self, path_to_bag: str = None,
                 metadata: Dict = None,
                 timestamp: datetime = None):
        # this is a super-sloppy __init__ method, sorry...
        self.path = path_to_bag
        if os.path.exists(path_to_bag):
            self._bag = bagit.Bag(path_to_bag)
        else:
            os.mkdir(path_to_bag)
            self._bag = bagit.make_bag(
                path_to_bag,
                {
                    **metadata,
                    **{"created": timestamp}
                })

    @property
    def bag(self):
        return self._bag

    @bag.setter
    def bag():
        raise Bagger.BagException("Please don't mess with the bag!")

    @property
    def data_path(self):
        return os.path.join(self.path, "data")

    def __enter__(self):
        return self.data_path

    def __exit__(self, type, value, traceback):
        self._bag.save(manifests=True)
