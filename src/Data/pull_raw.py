import datetime
import json
import logging
import os
from pathlib import Path
import re
import subprocess
import time
from typing import Dict, List

import click
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from src.Data.bagger import Bagger


logger = logging.getLogger(__name__)


def latest_download(downloads_dir):
    p1 = subprocess.Popen(["ls", f'{downloads_dir}', "-ct1"],
                          stdout=subprocess.PIPE)
    p2 = subprocess.Popen(
        ["head", "-1"], stdin=p1.stdout, stdout=subprocess.PIPE)
    latest = p2.communicate()[0] or 'NO_FILES'
    return latest


def wait_for_download(downloads_dir: str, timeout: float,
                      most_recent: str) -> float:
    """A bit of a hack to know when a download is finished."""
    start = time.monotonic()
    elapsed = start
    download_started = False
    wait = True
    while wait:
        elapsed = time.monotonic() - start
        if elapsed > timeout:
            raise TimeoutError
        try:
            filename = latest_download(downloads_dir).decode().replace(
                "\n", "")
        except AttributeError:
            # I don't understand why it somtimes throws an
            # exception, nor do I understand why it apparently
            # makes no difference...
            # For what it's worth, the output of latest_download is
            # sometimes a str, the 'NO_FILES' output.
            # The timeout parameter will break us out of this loop,
            # if necessary.
            filename = ".crdownload"
        is_downloading = filename.endswith(".crdownload")
        if is_downloading:
            download_started = True
        if download_started and not is_downloading:
            logger.info(f"Download complete for {filename}")
            # print(filename)
            wait = False
    # logging.info(f"{filename} downloaded in {elapsed} seconds")
    return elapsed


class HTTPDownloader:
    """Uses requests to download files thru HTTP"""

    def __init__(self,
                 file_handler: logging.FileHandler = None):
        self.file_handler = file_handler
        self.report_info_data = []

    def log_report_info(self, report_info: Dict):
        self.report_info_data.append(report_info)
        if not self.file_handler and not logger.handlers:
            logger.warn("LOGGING FILE HANDLER NOT SET!!")
            # raise ValueError("LOGGING FILE HANDLER NOT SET!!")
        logger.info(json.dumps(report_info))

    def get_pdf_files(self,
                      urls: List,
                      bag: str = None,
                      test: bool = False,
                      **kwargs) -> None:
        downloads_dir = bag or os.getenv("RAW_DATA_DOWNLOADS_DIR")
        downloads_subdir = os.path.join(
            downloads_dir, "CoreTrustSeal_PDF_files")
        if not os.path.exists(downloads_subdir):
            os.mkdir(downloads_subdir)
        if test:
            urls = urls[:5]
        for url in urls:
            log_data = url
            url = url["href"]
            r = requests.get(url)
            if r:
                filename = r.url.split("/")[-1]
                filepath = os.path.join(
                    downloads_subdir, filename)
                with open(filepath, 'wb') as f:
                    f.write(r.content)
                print(f"PDF data written to {filepath}")
                log_data["downloaded"] = True
                log_data["filepath"] = filepath
                self.log_report_info(log_data)
            else:
                # TODO: log properly
                print(f"{r.status} -- {r.reason}")
            # Sleep for 2 seconds so we don't get locked out
            time.sleep(2)


class FileDataRetrieval:
    """Classmethods using Selenium browser automation to extract raw data."""

    # Constants
    DSA_DB_FILENAME = "dsatool_dump20171129.sql"
    DSA_DATA_URL = "https://easy.dans.knaw.nl/ui/datasets/id/easy-dataset:116038/tab/2"
    CTS_DATA_URL = "https://www.coretrustseal.org/why-certification/certified-repositories/"

    def __init__(self,
                 chrome_exec_path: str,
                 options: webdriver.ChromeOptions = None,
                 file_handler: logging.FileHandler = None):
        self.driver = webdriver.Chrome(
            chrome_exec_path,
            options=options
        )
        self.file_handler = file_handler

    @classmethod
    def with_options(cls,
                     chrome_exec_path: str,
                     downloads_dir: str,
                     headless: bool = True,
                     file_handler: logging.FileHandler = None,
                     **kwargs):
        opts = webdriver.ChromeOptions()
        if headless:
            opts.add_argument("headless")
        downloads_abspath = os.path.join(
            os.path.abspath(os.getcwd()),
            downloads_dir
        )
        prefs = {"download.default_directory": downloads_abspath}
        opts.add_experimental_option("prefs", prefs)
        # opts.binary_location = chrome_exec_path
        if file_handler:
            logger.addHandler(file_handler)
        return cls(chrome_exec_path, opts)

    @classmethod
    def get_sql(cls, bag: str = None, **kwargs):
        """Downloads MySQL dump file"""
        downloads_dir = bag or os.getenv("RAW_DATA_DOWNLOADS_DIR")
        print(f"GET SQL: DOWNLOADS DIR: {downloads_dir}")
        inst = cls.with_options(
            chrome_exec_path="lib/chromedriver",#os.getenv("CHROME_EXEC_PATH"),
            downloads_dir=downloads_dir,
            **kwargs
        )
        inst.driver.get(cls.DSA_DATA_URL)
        try:
            db_download = WebDriverWait(inst.driver, 15).until(
                EC.presence_of_element_located(
                    (By.PARTIAL_LINK_TEXT, cls.DSA_DB_FILENAME)))
            db_download.click()
            elapsed = wait_for_download(
                downloads_dir,
                30,
                latest_download(downloads_dir)
            )
            sqldump_file = latest_download(bag).decode()
            sqldump_file = sqldump_file.replace("\n", "")
            return sqldump_file
        except Exception as e:
            print(e)
            raise e

    def log_report_info(self, report_info: Dict):
        if not self.file_handler and not logger.handlers:
            raise ValueError("LOGGING FILE HANDLER NOT SET!!")
        logger.info(json.dumps(report_info))

    @classmethod
    def get_pdf_urls(cls, bag: str = None, **kwargs) -> List:
        """Downloads PDF files"""
        downloads_dir = bag or os.getenv("RAW_DATA_DOWNLOADS_DIR")
        # TODO: Find out if pipenv automatically sets the CWD as the
        # project root?
        chrome_exec_path = os.path.join(
            os.getcwd(),
            os.getenv("CHROME_EXEC_PATH")
        )
        inst = cls.with_options(
            chrome_exec_path=chrome_exec_path,
            downloads_dir=downloads_dir,
            headless=True,
            **kwargs
        )
        inst.driver.get(cls.CTS_DATA_URL)
        WebDriverWait(inst.driver, 300).until(
            EC.visibility_of_element_located(
                (By.CLASS_NAME, "mmp-list-page")))
        pg_num = inst.driver.find_element_by_class_name("mmp-list-page")
        pg_total = re.search(r"([0-9]+)", pg_num.text).group(1)
        print(pg_total)
        report_urls = []
        for i in range(int(pg_total)):
            current_pg = i + 1
            if current_pg > 1:
                pg_num = inst.driver.find_element_by_class_name(
                    "mmp-list-page")
                pg_input = pg_num.find_element_by_tag_name("input")
                pg_input.clear()
                pg_input.send_keys(str(current_pg))
                pg_input.send_keys(Keys.ENTER)
                WebDriverWait(inst.driver, 15).until(
                    EC.visibility_of_element_located(
                        (By.CLASS_NAME, "mmp-list")))
            reports = inst.driver.find_elements_by_partial_link_text(
                "CoreTrustSeal certification 2017-2019")
            for report in reports:
                href = report.get_attribute("href")
                parent = report.find_element(By.XPATH, "..")
                website = parent.find_element(By.XPATH, "./*")
                website = website.get_attribute("href")
                report_info = {"website": website, "href": href}
                report_urls.append(report_info)
        return report_urls

    @staticmethod
    def get_pdf_files(urls: List,
                      bag: str = None,
                      file_handler: logging.FileHandler = None,
                      manifest_filepath: str = None,
                      **kwargs):
        downloader = HTTPDownloader(file_handler)
        downloader.get_pdf_files(urls, bag, **kwargs)
        if manifest_filepath:
            data = {"pdf_file_info": downloader.report_info_data}
            with open(manifest_filepath, "w") as f:
                f.write(json.dumps(data))
            logger.info(
                f"PDF file information recorded at {manifest_filepath}")
