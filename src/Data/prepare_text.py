from datetime import datetime
from functools import reduce
import os
import traceback
from typing import List, Dict

from bs4 import BeautifulSoup
import click
import nltk
import pandas as pd
import spacy

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.engine import reflection
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, aliased

from src.Data import text_models
from src.Data.make_dataset import MySQLManager


class Tables:

    def __init__(self):
        pass

    def __getitem__(self, item):
        return self.__dict__[item]


class DataTransfer:

    def __init__(self, driver="mysql+pymysql://"):
        self.driver = driver
        mysql_manager = MySQLManager.from_env()
        self.user = mysql_manager.user
        self.password = mysql_manager.password
        self.db_name = mysql_manager.db_name
        self.host = mysql_manager.host or "localhost"
        self.port = mysql_manager.port or 3306
        self.engine = None
        self.session = None
        self.meta = None
        self.Base = None
        self.tables = Tables()

    def make_engine(self):
        connect_string = "".join([
            f"{self.driver}{self.user}:{self.password}",
            f"@{self.host}:{self.port}/{self.db_name}"
        ])
        return create_engine(connect_string)

    def load_meta(self, engine):
        return MetaData(bind=engine, reflect=True)

    def make_tables(self):
        for tbl in self.meta.tables.values():
            t = type(tbl.name,
                     (self.Base,),
                     {"__table__": tbl})
            self.tables.__dict__[tbl.name] = t

    @classmethod
    def make(cls, **kwargs):
        inst = cls(**kwargs)
        inst.engine = inst.make_engine()
        inst.meta = inst.load_meta(inst.engine)
        inst.Base = declarative_base(metadata=inst.meta)
        inst.make_tables()
        Session = sessionmaker(bind=inst.engine)
        inst.session = Session()
        return inst


class DataCleaner:

    def __init__(self, data_transfer=DataTransfer.make()):
        self._data_transfer = data_transfer
        self.tables = data_transfer.tables
        self.session = data_transfer.session
        self.tokenizer = nltk.tokenize.WordPunctTokenizer()
        self.stemmer = nltk.stem.PorterStemmer()
        #self.stopwords = set(nltk.corpus.stopwords.words('english'))
        # loading spacy
        self.nlp = spacy.load("en_core_web_sm")

    @staticmethod
    def strip_markup(markup: str):
        soup = BeautifulSoup(markup)
        return "".join(soup.strings)

    def clean_column_text(self, table: str, column: str):
        rows = [
            row._asdict()[column] for row
            in
            self.session.query(
                self.tables[table].__dict__[column]).all()
        ]
        return [self.strip_markup(markup) for markup in rows]

    def perform_preprocessing_nltk(self, text):
        tokens = [
            token for token
            in self.tokenizer.tokenize(text)
            if token not in self.stopwords
            ]
        return [self.stemmer.stem(token) for token in tokens]

    def perform_preprocessing_spacy(self, text):
        doc = self.nlp(text)
        tokens = [i for i in doc if not i.is_stop]
        return [i.text for i in tokens if i.is_alpha]

    @staticmethod
    def row_columns(row, table_name=None) -> Dict:
        row_dict = {
            k: v for k, v in row.__dict__.items()
            if k[0] != "_"
        }
        if table_name:
            return {
                f"{table_name}.{k}": v
                for k, v in row_dict.items()
            }
        else:
            return row_dict


@click.group()
def cli():
    pass


def cts_repositories(output_filepath=None):
    pass


def assessment_compliance_scores(output_filepath=None):
    dc = DataCleaner()
    q1 = dc.session.query(
        dc.tables.assessment_data_entry.id,
        dc.tables.assessment_data_entry.statement_of_compliance_id,
        dc.tables.assessment_data_entry.version_no,
        dc.tables.assessment_data_entry.creation_date,
        dc.tables.assessment_data_entry.guideline_id,
        dc.tables.assessment_data_assessment.id,
        dc.tables.assessment_data_assessment.repository_id,
        dc.tables.assessment_data_assessment.guidelinepackage_id,
        dc.tables.dsa_data_guidelinepackage.name
    ).join(
        dc.tables.assessment_data_assessment,
        dc.tables.assessment_data_entry.assessment_id\
        ==dc.tables.assessment_data_assessment.repository_id
    ).join(
        dc.tables.dsa_data_guidelinepackage,
        dc.tables.assessment_data_assessment.guidelinepackage_id\
        ==dc.tables.dsa_data_guidelinepackage.id
    )
    q2 = dc.session.query(
        dc.tables.contact_data_repository.id,
        dc.tables.contact_data_repository.title
    )
    ids, names = (pd.DataFrame([i._asdict() for i in q1]),
                  pd.DataFrame([i._asdict() for i in q2]))
    scores = ids.merge(
        names, how="inner",
        left_on="repository_id",
        right_on="id"
    ).drop(["id_y"], axis=1)
    if output_filepath:
        scores.to_csv(output_filepath)
    return scores


def score_diff(df: pd.DataFrame) -> pd.DataFrame:
    changed = pd.DataFrame(columns=[
        "repository_id",
        "repository_name",
        "cummulative_score",
        "assessment_id",
        "guidelines"
    ])
    repos = df.repository_id.unique()
    for repo in repos:
        repo_data = df[df["repository_id"] == repo]
        assmts = repo_data.id_x.unique()
        for assmt_id in assmts:
            assmt = df[df.id_x == assmt_id]
            most_recent_scores = [
                assmt[assmt.guideline_id == i].sort_values(
                    by="creation_date", ascending=False
                ).iloc[0].statement_of_compliance_id
                for i in assmt.guideline_id.unique()
            ]
            cum_score = reduce(lambda x, y: x + y, most_recent_scores)
            r_id = repo_data["repository_id"]
            # unpacking tuple, will throw exc if not unique
            r_id, = r_id.unique()
            r_name, = repo_data["title"].unique()
            guidelines, = assmt["name"].unique()
            row = {
                "repository_id": r_id, # repo_data["repository_id"][0],
                "repository_name": r_name,
                "cummulative_score": cum_score,
                "assessment_id": assmt_id,
                "creation_date": list(assmt.sort_values(
                    by="creation_date", ascending=False).creation_date)[0],
                "guidelines": guidelines
            }
            changed = changed.append(row, ignore_index=True)
    return changed


def multiple_reports(df: pd.DataFrame) -> pd.DataFrame:
    ids = list(df.repository_id)
    multi = [i for i in ids if ids.count(i) > 1]
    return df[df.repository_id.isin(multi)]


def assessment_compliance_scores_(output_filepath=None):
    dc = DataCleaner()
    subq = dc.session.query(
        dc.tables.contact_data_repository.title
    ).join(
        dc.tables.assessment_data_assessment,
        dc.tables.contact_data_repository.id\
        ==dc.tables.assessment_data_assessment.repository_id
    ).subquery()
    repo_names = aliased(
        dc.tables.contact_data_repository, subq)
    query = dc.session.query(
        repo_names.title,
        dc.tables.assessment_data_entry.statement_of_compliance_id,
        dc.tables.assessment_data_entry.version_no,
        dc.tables.assessment_data_entry.creation_date,
        dc.tables.assessment_data_assessment.repository_id
    ).join(
        dc.tables.assessment_data_assessment,
        dc.tables.assessment_data_assessment.id\
        ==dc.tables.assessment_data_entry.assessment_id
    )
    return query


#@click.command()
#@click.option("-o", "--output-filepath", default=None)
def assessment_document_view(output_filepath=None):
    dc = DataCleaner()
    query = dc.session.query(
        dc.tables.assessment_data_assessment,
        dc.tables.contact_data_repository,
        #dc.tables.assessment_data_entry,
        #dc.tables.dsa_data_guideline,
        dc.tables.dsa_data_guidelinepackage
    ).join(
        dc.tables.dsa_data_guidelinepackage,
        dc.tables.assessment_data_assessment.guidelinepackage_id\
        == dc.tables.dsa_data_guidelinepackage.id
    ).join(
        dc.tables.contact_data_repository,
        dc.tables.assessment_data_assessment.repository_id\
        == dc.tables.contact_data_repository.id
    ).all()
    df = pd.DataFrame([
        reduce(
            lambda x, y: {**x, **y},
            [dc.row_columns(tr, tr.__table__.name) for tr in row]
        )
        for row in query
    ])
    return df


def get_repos_per_version():
    """Returns a dict containing the set of repositories per version."""
    scores = assessment_document_view()
    repos = scores[["contact_data_repository.id",
                    "assessment_data_assessment.guidelinepackage_id"]]
    dsa_2010 = repos[repos[
        "assessment_data_assessment.guidelinepackage_id"] == 1]
    dsa_2014 = repos[repos[
        "assessment_data_assessment.guidelinepackage_id"] == 52]
    cts = repos[
        repos["assessment_data_assessment.guidelinepackage_id"] == 54]
    repo_ids = {
        "dsa_2010": {i for i in dsa_2010[
            "contact_data_repository.id"].unique()},
        "dsa_2014": {i for i in dsa_2014[
            "contact_data_repository.id"].unique()},
        "cts": {i for i in cts[
            "contact_data_repository.id"].unique()}
    }
    dc = DataCleaner()
    repo_names = {
        k: [dc.session.query(dc.tables.contact_data_repository).filter(
            dc.tables.contact_data_repository.id==int(i)).first().title
            for i in repo_ids[k]]
        for k in repo_ids.keys()
        }
    return {k: sorted(v) for k, v in repo_names.items()}


def repo_versions_df(repos_per_version: Dict, output_filepath=None):
    """Returns a DataFrame with boolean values for each repo:version pair."""
    repos = get_repos_per_version()
    repos_set = sorted(reduce(
        lambda x, y: set(x).union(y), repos.values()))
    df = pd.DataFrame({
        "repository_name": [i for i in repos_set],
        "has_dsa_2010": [bool(i in repos["dsa_2010"]) for i in repos_set],
        "has_dsa_2014": [bool(i in repos["dsa_2014"]) for i in repos_set],
        "has_cts": [bool(i in repos["cts"]) for i in repos_set]
    })
    if output_filepath:
        df.to_csv(output_filepath)
    return df


def build_cts_comparison(bagit_dir=None, data_dir=None,
                         save_csv=False) -> pd.DataFrame:
    """Builds a table that can be used to de-duplicate
documents among the downloaded reports and database reports."""
    cts_files = "CoreTrustSeal_PDF_files"
    if not bagit_dir and not data_dir:
        name_root = os.environ.get("RAW_DATA_DOWNLOADS_DIR")
        raw_data = os.path.join(*os.path.split(name_root)[:-1])
        dirs = [d for d in os.listdir(raw_data)
                if os.path.split(name_root)[-1] in d]
        most_recent = sorted(dirs)[-1]
        data_dir = os.path.join(raw_data,
                                most_recent, "data", cts_files)
    elif not data_dir:
        data_dir = os.path.join(bagit_dir, "data", cts_files)
    downloads = pd.DataFrame({
        "file_name": [f for f in os.listdir(data_dir)],
        "has_cts_downloaded": [True for f in os.listdir(data_dir)]
    })
    db = repo_versions_df(get_repos_per_version())
    db["has_cts_db"] = db["has_cts"]
    db = db[["repository_name", "has_cts_db"]]
    downloads = pd.concat([downloads, db], axis=1)
    sorted_file_names = downloads["file_name"].sort_values()
    print(sorted_file_names)
    downloads["some_file_name"] = sorted_file_names
    print(downloads)
    if save_csv:
        fname = "downloads_database_comparison_{}.csv".format(
            datetime.now().strftime('%FT%T'))
        fpath = os.path.join("data", "interim", fname)
        downloads.to_csv(fpath, index=False)
        print(f"File written to {fpath}")
    return (downloads, db)


def cts_guidelines():
    dc = DataCleaner()
    d = dc.session.query(
        dc.tables.dsa_data_order.order_number,
        dc.tables.dsa_data_order.guideline_id,
        dc.tables.dsa_data_guideline.title,
        dc.tables.dsa_data_guidelinepackage.name
    ).join(
        dc.tables.dsa_data_guideline,
        dc.tables.dsa_data_guideline.id==\
        dc.tables.dsa_data_order.guideline_id
    ).join(
        dc.tables.dsa_data_guidelinepackage,
        dc.tables.dsa_data_order.guidelinepackage_id==\
        dc.tables.dsa_data_guidelinepackage.id
    ).filter(
        dc.tables.dsa_data_guidelinepackage.id==54).all()
    return d


@click.command()
@click.option("-o", "--output-filepath")
@click.option("-f", "--filter-fn")
def cummulative_scores(output_filepath, filter_fn):
    callbacks = {
        "multiple_reports": multiple_reports
    }
    data = score_diff(assessment_compliance_scores())
    if filter_fn:
        data = callbacks[filter_fn](data)
    if output_filepath:
        data.to_csv(output_filepath, index=False)
    else:
        click.echo(data)


@click.command()
@click.option("-t", "--table", "table")
@click.option("-c", "--columns")
def to_csv(table, columns):
    dc = DataCleaner()
    try:
        tbl = dc.tables[table]
    except KeyError:
        print(traceback.format_exc())
        click.echo(f"SCRIPT BUNGLED: table {table} not found!")
        return None
    try:
        if columns:
            cols = [
                tbl.__dict__[c]
                for c in
                columns.split(",")
            ]
            query = dc.session.query(*cols).all()
            print(type(query))
            df = pd.DataFrame([
                {k: v for k, v in row._asdict().items()}
                for row in query
            ])
        else:
            query = dc.session.query(tbl).all()
            df = pd.DataFrame([
                {
                    k: v for k, v in row.__dict__
                    if k[0] != "_"
                }
                for row in query
            ])
        print(df)
    except Exception:
        print(traceback.format_exc())
        return None


cli.add_command(to_csv)
cli.add_command(cummulative_scores)

if __name__ == "__main__":
    cli()
