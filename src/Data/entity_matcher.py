import csv
import json
import logging
import os

import pandas as pd

from src.Data.constants import PKG_LOOKUP, SECTIONS_LOOKUP
from src.Data.database_config import DatabaseConfig
from src.Data.database_migration import MigrationFactory
import src.Data.text_models as models
from src.utils import remove_trailing_ws, read_csv

logger = logging.getLogger(__name__)


class EntityMatcher:

    def __init__(self,
                 json_filepath=None,
                 sqlite_filepath=None,
                 success_match_filepath=None,
                 failure_match_filepath=None,
                 failure_report_filepath=None,
                 file_handler=None):
        self.json_filepath = json_filepath
        self.sqlite_filepath = sqlite_filepath
        self.failure_report_filepath = failure_report_filepath
        self.success_match_filepath = success_match_filepath
        self.failure_match_filepath = failure_match_filepath
        self.assessments = None
        self.migration = None
        self.matched_db = []
        self.matched_json = []
        self.unmatched = []
        self.file_handler = file_handler

    def load_assessments(self):
        with open(self.json_filepath, "r") as f:
            self.assessments = json.loads(f.read())

    def build_migration(self):
        migration = MigrationFactory.make_migration(
            sqlite_db_filepath=self.sqlite_filepath,
            existing_sqlite_db=True)

        @migration.sqlite()
        def get_existing_repositories(session):
            output = []
            unmatched = []
            for assmt in self.assessments["2017_2019"]:
                existing = session.query(
                    models.Repository
                    ).filter_by(
                        website=assmt["website"]
                    ).first()
                if not existing \
                   and assmt["website"] != assmt["pdf_website"]:
                    # just double-checking to make sure
                    # pesky artifacts are removed!
                    alt_site = remove_trailing_ws(assmt["pdf_website"])
                    existing = session.query(
                        models.Repository
                    ).filter_by(
                        website=alt_site
                    ).first()
              #  elif not existing:
              #      repo_name = remove_trailing_ws(assmt["name"])
              #      existing = session.query(
              #          models.Repository
              #      ).filter_by(
              #          name=repo_name
              #      ).first()
              #      if existing:
              #          if existing.id not in [
              #                  i.repository_id
              #                  for i in session.query(models.Assessment).all()
              #          ]:
              #              existing = None
                if existing:
                    new_assmt = models.Assessment(
                        # seal_date=assmt["seal_date"],
                        dsa_db_guidelinepackage_id=54,
                        guideline_package_id=3,
                        repository_id=existing.id,
                        dsa_db_repository_id=existing.dsa_db_id,
                        repository_uuid=assmt["repository_uuid"]
                    )
                    self.matched_db.append(existing)
                    self.matched_json.append(assmt["website"])
                    output.append({
                        "assmt": new_assmt,
                        "entries": assmt["entries"]
                    })
                else:
                    unmatched.append(assmt)
            return {"matched": output, "unmatched": unmatched}

        @migration.sqlite()
        def process_matched(session, matched, unmatched):
            output = []
            uuids = []
            for assmt in matched:
                assmt_rec = assmt["assmt"]
                existing_assmt = session.query(
                    models.Assessment
                ).filter_by(
                    guideline_package_id=3
                ).filter_by(
                    repository_id=assmt_rec.repository_id
                ).first()
                if not existing_assmt:
                    session.add(assmt_rec)
                else:
                    logger.info(
                        "Existing CTS 2017-2019 assessment in DSA database")
                    continue
                session.flush()
                logger.info(
                    f"INSERT: assessment.id = {assmt_rec.id}")
                # adding entries
                for sec, entry in assmt["entries"].items():
                    guideline_id = SECTIONS_LOOKUP[sec]
                    new_entry = models.AssessmentEntry(
                        compliance_level=entry["compliance_level"],
                        response=entry["response"],
                        assessment_id=assmt_rec.id,
                        guideline_id=guideline_id
                        )
                    session.add(new_entry)
                    logger.info(f"INSERT: {sec}")
                # adding assmt_rec of successfully ingested
                output.append(assmt_rec)
                uuids.append(assmt_rec.repository_uuid)
            session.commit()
            return {
                "matched": output,
                "unmatched": unmatched,
                "matched_uuids": uuids
            }

        @migration.sqlite()
        def create_unmatched_success_table(session,
                                           matched,
                                           unmatched,
                                           matched_uuids):
            all_repos = session.query(models.Repository).all()
            matched_ids = set([i.repository_id for i in matched])
            cts_assmts = session.query(models.Assessment).filter_by(
                guideline_package_id=3
            ).all()
            has_cts = set([i.repository_id for i in cts_assmts])
            has_cts = has_cts.union(matched_ids)
            possible_matches = [
                {"id": i.id, "website": i.website, "name": i.name}
                for i in all_repos
                if i.id not in has_cts
            ]
            df_x = pd.DataFrame(possible_matches,
                                columns=["id", "website", "name"])
            df_x = df_x.sort_values(by=["name"])

            pdf_possible_matches = [
                i for i in self.assessments["2017_2019"]
                if i["repository_uuid"] not in matched_uuids
            ]
            try:
                pdf_possible_matches = [
                    {
                        "pdf_repository_name": i["name"],
                        "pdf_website": i["website"],
                        "repository_uuid": i["repository_uuid"],
                        "match_on_id": None
                    } for i in pdf_possible_matches
                ]
            except TypeError as e:
                print(e)
                print(len(pdf_possible_matches))
                print(pdf_possible_matches)
            df_y = pd.DataFrame(
                pdf_possible_matches,
                columns=["pdf_repository_name",
                         "pdf_website",
                         "repository_uuid",
                         "match_on_id"]
                )
            df_y = df_y.sort_values(by=["pdf_repository_name"])
            df_y = df_y.reset_index(drop=True)
            df_x = df_x.reset_index(drop=True)
            print(df_y)

            df = pd.concat([df_x, df_y], ignore_index=True, axis=1)
            print(df)
            df.to_csv(self.success_match_filepath, index=False,
                      header=["id",
                              "dsa_website",
                              "dsa_name",
                              "pdf_name",
                              "pdf_website",
                              "pdf_uuid",
                              "match_on_id"],
                      quoting=csv.QUOTE_NONNUMERIC)

        @migration.sqlite()
        def create_unmatched_failure_table(session):
            all_repos = session.query(models.Repository).all()
            cts_assmts = session.query(
                models.Assessment
            ).filter_by(guideline_package_id=3).all()
            repos_w_cts_assmt = [i.repository_id for i in cts_assmts]
            cts_repos = [
                i for i in all_repos
                if i.id in repos_w_cts_assmt
            ]
            df_x = pd.DataFrame(
                [
                    {"id": i.id, "website": i.website, "name": i.name}
                    for i in cts_repos
                ],
                columns=["id", "website", "name"]
            )
            df_y = pd.read_csv(self.failure_report_filepath)
            df_y["pdf_name"] = [
                os.path.split(i)[-1].replace("-", " ")
                for i in df_y["filepath"]
            ]
            matched = map(
                lambda x: x["website"] in [
                    row["website"] for idx, row in df_x.iterrows()
                ]
                or x["pdf_name"] in [
                    row["name"] for idx, row in df_x.iterrows()
                ],
                [row for idx, row in df_y.iterrows()]
            )
            matched = list(matched)
            df_y["matched"] = matched
            logger.info(f"{len(matched)} matches found")
            df_y = df_y[df_y["matched"] == False]
            df_y = df_y[["pdf_name", "website", "filepath"]]

            df = pd.concat([df_x, df_y], ignore_index=True, axis=1)
            df.to_csv(self.failure_match_filepath, index=False,
                      quoting=csv.QUOTE_NONNUMERIC,
                      header=[
                          "id",
                          "website",
                          "name",
                          "pdf_name",
                          "website",
                          "filepath"
                      ])

        migration.add_steps(get_existing_repositories,
                            process_matched,
                            create_unmatched_success_table,
                            create_unmatched_failure_table)

        self.migration = migration

    def build_matched_entities_migration(self):
        migration = MigrationFactory.make_migration(
            sqlite_db_filepath=self.sqlite_filepath,
            existing_sqlite_db=True)

        @migration.sqlite()
        def load_matched(session):
            data = read_csv(self.success_match_filepath)
            output = []
            for row in data:
                if not row['match_on_id']:
                    if row["pdf_uuid"]:
                        output.append(row)
                else:
                    assmt_data, = [
                        assmt for assmt in
                        self.assessments["2017_2019"]
                        if assmt["repository_uuid"] == row["pdf_uuid"]
                    ]
                    dsa_db_repo_id = session.query(
                        models.Repository
                    ).filter_by(
                        id=row["match_on_id"]
                    ).first().dsa_db_id
                    assmt = models.Assessment(
                        repository_id=row['match_on_id'],
                        #seal_date=assmt_data["seal_date"],
                        guideline_package_id=3,
                        dsa_db_guidelinepackage_id=54,
                        dsa_db_repository_id=dsa_db_repo_id
                    )
                    session.add(assmt)
                    session.flush()
                    logger.info(
                        f"INSERT: {assmt.id} for {row['pdf_name']}")

                    for section, entry in assmt_data["entries"].items():
                        guideline_id = SECTIONS_LOOKUP[section]
                        try:
                            entry_row = models.AssessmentEntry(
                                compliance_level=entry["compliance_level"],
                                response=entry["response"],
                                assessment_id=assmt.id,
                                guideline_id=guideline_id
                            )
                        except TypeError:
                            logger.warning(
                                f"Null assessment entry in {row['pdf_name']}")
                            entry_row = models.AssessmentEntry(
                                assessment_id=assmt.id,
                                guideline_id=guideline_id
                                )
                        session.add(entry_row)
                        logger.info(
                            f"INSERT: {section} on Assessment {assmt.id}")
            session.commit()
            return {"unmatched": output}

        @migration.sqlite()
        def load_unmatched(session, unmatched):
            for row in unmatched:
                repo = models.Repository(
                    name=row["pdf_name"], website=row["pdf_website"])
                session.add(repo)
                session.flush()

                assmt_data, = [
                    i for i in self.assessments["2017_2019"]
                    if i["repository_uuid"] == row["pdf_uuid"]
                    ]
                assmt = models.Assessment(
                    #seal_date=assmt_data["seal_date"],
                    guideline_package_id=3,
                    dsa_db_guidelinepackage_id=54,
                    repository_id=repo.id
                )
                session.add(assmt)
                session.flush()

                for section, entry in assmt_data["entries"].items():
                    guideline_id = SECTIONS_LOOKUP[section]
                    try:
                        entry_row = models.AssessmentEntry(
                            compliance_level=entry["compliance_level"],
                            response=entry["response"],
                            assessment_id=assmt.id,
                            guideline_id=guideline_id
                        )
                    except TypeError:
                        logger.warning(
                            f"Null assessment entry in {row['pdf_name']}")
                        entry_row = models.AssessmentEntry(
                            assessment_id=assmt.id,
                            guideline_id=guideline_id
                            )
                    session.add(entry_row)
                    logger.info(
                        f"INSERT: {section} on Assessment {assmt.id}")

            session.commit()

        migration.add_steps(load_matched, load_unmatched)
        self.migration = migration

    @classmethod
    def make(cls,
             json_filepath=None,
             sqlite_filepath=None,
             success_match_filepath=None,
             failure_match_filepath=None,
             failure_report_filepath=None,
             file_handler=None):
        inst = cls(json_filepath,
                   sqlite_filepath,
                   failure_report_filepath=failure_report_filepath,
                   success_match_filepath=success_match_filepath,
                   failure_match_filepath=failure_match_filepath,
                   file_handler=file_handler
                   )
        logger.addHandler(file_handler)
        logger.info(f"sqlite_filepath: {sqlite_filepath}")
        logger.info(f"success_match_filepath: {success_match_filepath}")
        inst.load_assessments()
        return inst

    def generate_matching_tables(self):
        self.migration.run()
