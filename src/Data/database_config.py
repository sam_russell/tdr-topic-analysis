import os
from typing import Dict


class DatabaseConfig:
    """Struct for keys/values needed to create and use databases"""
    def __init__(self,
                 user=None,
                 password=None,
                 db_name=None,
                 db_host=None,
                 db_port=None,
                 db_filepath=None,
                 **kwargs):
        self.user = user
        self.password = password
        self.db_name = db_name
        self.db_host = db_host
        self.db_port = db_port
        self.db_filepath = db_filepath

    @classmethod
    def from_env(cls, env_map: Dict):
        kwargs = {k: os.environ.get(v) for k, v in env_map.items()}
        inst = cls(**kwargs)
        return inst

    def as_dict(self):
        return {k: v for k, v in self.__dict__.items() if k[0] != "_"}
