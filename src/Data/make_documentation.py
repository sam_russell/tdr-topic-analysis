import os
import subprocess

import click

TABLES = [
    # "admin_tools_dashboard_preferences",
    # "admin_tools_menu_bookmark",
    "assessment_data_assessment",
    "assessment_data_entry",
    "assessment_data_objection",
    "assessment_data_objection_guidelines",
    "assessment_data_permission",
    "assessment_data_reviewerentry",
    "assessment_data_secondreviewerentry",
    # "auth_group",
    # "auth_group_permissions",
    # "auth_message",
    # "auth_permission",
    # "axes_accessattempt",
    # "axes_accesslog",
    "contact_data_organization",
    "contact_data_repository",
    "contact_data_repository_organization",
    "contact_data_user",
    "contact_data_user_groups",
    "contact_data_user_organization",
    # "contact_data_user_user_permissions",
    # "content_menu",
    # "django_admin_log",
    # "django_content_type",
    # "django_cron_cronjoblog",
    # "django_flatpage",
    # "django_flatpage_sites",
    # "django_session",
    # "django_site",
    "dsa_data_guideline",
    "dsa_data_guidelinepackage",
    "dsa_data_guidelineschangelog",
    "dsa_data_order",
    "dsa_data_point",
    # "notifications_notification",
    # "notifications_notificationtype",
    # "south_migrationhistory"
    ]


@click.command()
@click.option("--target-filepath",
              default=os.path.join(
                  os.getcwd(),
                  "references",
                  "data_seal_db_doc.org"
              ))
@click.option("--output-style", default="org")
def build_db_doc(target_filepath: str, output_style):
    with open(target_filepath, "w+") as f:
        content = "\n".join(
            [
                "* Data Seal MySQL Database Documentation",
                "** Table Descriptions\n"
            ]
        )
        f.write(content)
    user = os.getenv("MYSQL_USERNAME")
    password = os.getenv("MYSQL_PASSWORD")
    db_name = os.getenv("MYSQL_DB_NAME")
    commands = {tbl: f"DESCRIBE {tbl};" for tbl in TABLES}
    for tbl_name, cmd in commands.items():
        p = subprocess.Popen(
            [
                "mysql",
                f"--user={user}",
                f"--password={password}",
                f"--database={db_name}",
                f"--execute={cmd}"
            ],
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE
        )
        output = p.communicate()[0].decode()
        if output_style == "org":
            output = output.replace("\t", "|")
            output = output.replace("\n", "\n|")
            output = output[:-1]
            rows = output.split("\n")
            output = [f"|{rows[0]}|"]
            output.append("|-")
            for i in rows[1:]:
                output.append(i)
            output = "\n".join(output)
        elif output_style == "csv":
            output = output.replace("\t", ",")
            if tbl_name != TABLES[0]:
                rows = output.split("\n")
                output = "\n".join(rows[1:])
        print(output)
        org_header = f"*** {tbl_name}\n{output}"
        with open(target_filepath, "a") as f:
            f.write(org_header)


if __name__ == "__main__":
    build_db_doc()
