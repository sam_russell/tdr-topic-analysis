import csv
from datetime import datetime
from functools import reduce, partial
import io
import json
import logging
import os
import re
import subprocess
import tempfile
from typing import Callable, Dict, List
from uuid import uuid4

from lxml import etree

from src.utils import month_num, dir_or_create, \
    remove_trailing_ws
from src.Data.bagger import Bagger

EOL = "\n".encode("UTF-8")

logger = logging.getLogger(__name__)


def bad_utf(bytes_obj: bytes) -> bool:
    """Returns True if a bytes object has bad UTF-8 chars"""
    try:
        bytes_obj.decode("utf-8")
        return False
    except UnicodeDecodeError:
        return True


def clean_bad_chars(xml_bytes: bytes) -> bytes:
    lines = xml_bytes.split(EOL)
    clean_lines = [
        good_utf for good_utf
        in
        [ln for ln in lines if not bad_utf(ln)]
        if not re.search(r"fontspec", good_utf.decode("utf-8"))
        ]
    return EOL.join(clean_lines)


def pdf_to_xml(pdf_filepath, output_filepath):
    """Converts PDF to XML file, returns STDOUT, STDERR"""
    p = subprocess.Popen(
        [
            "pdftohtml",
            "-xml",
            "-i",
            pdf_filepath,
            output_filepath
        ],
        stderr=subprocess.STDOUT
    )
    return p.communicate()


def xml_to_etree(xml_filepath) -> etree._Element:
    with open(xml_filepath, "rb") as f:
        xml_bytes = f.read()
    return etree.XML(clean_bad_chars(xml_bytes))


def make_xml_filename(pdf_filepath) -> str:
    pdf_filename = os.path.split(pdf_filepath)[-1].split(
        ".pdf")[0]
    return f"{pdf_filename.split('.xml')[0]}.xml"


def pdf_to_etree(pdf_filepath, xml_dir=None, **kwargs):
    xml_filepath = os.path.join(
        xml_dir,
        make_xml_filename(pdf_filepath))
    pdf_to_xml(pdf_filepath, xml_filepath)
    return xml_to_etree(xml_filepath)


def extract_text_elements(doc: etree._Element) -> List:
    """Returns a list of text elements"""
    pages = [i.getchildren() for i in doc if i.tag == "page"]
    return list(filter(lambda x: x.tag == "text",
                       reduce(lambda x, y: x + y, pages)))


def filter_texts(texts: List, pred: Callable,
                 handler: Callable = None, *args, **kwargs) -> List:
    if handler:
        return handler([i for i in texts if pred(i)], **kwargs)
    else:
        return [i for i in texts if pred(i)]


def accumulate_lines(element_coll: List[etree._Element],
                     start_idx: int, pred: Callable) -> List:
    output = []
    element = element_coll[start_idx]
    condition = pred(element)
    # print(condition)
    it = iter(element_coll[(start_idx):])
    while condition is True:
        output.append(element)
        element = it.__next__()
        # print(f"{element_coll.index(element)} ... {element}")
        # print("".join(element.itertext()))
        condition = pred(element)
        # print(condition)
    return output


def find_repository_info(texts) -> Dict:
    """Extracts identifying info about the repository"""
    repository, = [i for i in texts if re.match(r"^Repository:.*",
                                                str(i.text))]
    r_name = repository.getnext().text
    # finding the website, this could be a useful unique identifier
    website, = [i for i in texts if re.search(r"Website:", str(i.text))]
    w_url, = website.getnext().find("a").values()
    cert_date, = [i for i in texts if re.search(r"Certification Date",
                                                str(i.text))]
    return {
        "repository_name": r_name,
        "website": w_url,
        "certification_date": "".join(cert_date.itertext())
    }


def find_assessment_sections(texts) -> Dict:
    bold = [i.find("b") for i in texts if i.find("b") is not None]
    sections = [i for i in [j for j in bold if j.text is not None]
                if re.match(r"^[IVX]+\..+", i.text)]
    return {
        re.search(
            r"[IVX]+\.\s(.+)",
            "".join(list(i.itertext()))).group(1): i.getparent()
        for i in sections
    }


def find_response(lines: List[str],
                  idx_hint=("Response:", "Reviewer Entry")) -> str:
    """Trims extraneous text from response section"""
    start = lines.index(idx_hint[0])
    print(start)
    from_start_idx = lines[start:]
    offset = len(lines) - len(from_start_idx)
    stop = from_start_idx.index(idx_hint[1])
    stop += offset
    print(stop)
    return "\n".join(lines[start:stop:])


def find_compliance_level(lines: List[str],
                          idx_hint="Compliance Level:") -> int:
    """Extracts the compliance level score as integer"""
    idx = lines.index(idx_hint)
    compliance = lines[idx + 1]
    lvl, = [char for char in compliance
            if re.match(r"[0-9]", char)]
    return int(lvl)


class AssessmentPDF:
    """Represents a PDF assessment"""
    def __init__(self, filepath, website):
        self.filepath = filepath
        self.website = website
        self.pdf_website = None
        self.pdf_repo_name = None
        self.cert_date = None
        self.extracted_data_filepath = None


class PDFPayload:
    """Contains data extracted from a PDF"""
    def __init__(self,
                 texts: Dict,
                 sections: Dict,
                 document: AssessmentPDF):
        self.texts = texts
        self.sections = sections
        self.document = document
        self.section_data = None


class PDFTextExtractor:

    def __init__(self,
                 file_manifest_path: str = None,
                 log_file_handler: logging.FileHandler = None,
                 reports_dir: str = None,
                 xml_dir: str = None,
                 output_filepath=None,
                 success_rpt_fpath=None,
                 failure_rpt_fpath=None):
        self.file_manifest_path = file_manifest_path
        self.file_manifest = None
        self.log_file_handler = log_file_handler
        self.reports_dir = reports_dir
        self.xml_dir = xml_dir
        self.output_filepath = output_filepath
        self.success_rpt_fpath = success_rpt_fpath
        self.failure_rpt_fpath = failure_rpt_fpath
        self.success = []
        self.failure = []

        print(f"__init__ {self.file_manifest_path}")

    def load_manifest(self):
        with open(self.file_manifest_path, "r") as f:
            self.file_manifest = json.loads(f.read())
            logger.info("Using file manifest loaded from {}".format(
                self.file_manifest_path))

    @classmethod
    def make(cls,
             file_manifest_path: str = None,
             log_file_handler: logging.FileHandler = None,
             xml_dir: str = None,
             *args, **kwargs):
        print(f"make {file_manifest_path}")
        inst = cls(file_manifest_path=file_manifest_path,
                   log_file_handler=log_file_handler,
                   xml_dir=xml_dir,
                   *args, **kwargs)
        logger.addHandler(inst.log_file_handler)
        inst.load_manifest()
        return inst

    def extract_data(self, elmt_tree: etree.Element,
                     assmt_pdf: AssessmentPDF) -> PDFPayload:
        fpath = assmt_pdf.filepath
        try:
            texts = extract_text_elements(elmt_tree)
        except Exception as e:
            logger.error(f"failed on extract_text_elements {fpath}")
            raise e
        try:
            sections = find_assessment_sections(texts)
        except Exception as e:
            logger.error(f"failed on extract_text_elements {fpath}")
            raise e
        try:
            info = find_repository_info(texts)
        except Exception as e:
            logger.error(f"failed on extract_text_elements {fpath}")
            raise e
        assmt_pdf.cert_date = info["certification_date"]
        assmt_pdf.pdf_website = info["website"]
        assmt_pdf.pdf_repo_name = info["repository_name"]
        return PDFPayload(texts, sections, assmt_pdf)

    def load_pdf(self, assmt_pdf: AssessmentPDF) -> PDFPayload:
        try:
            elmt_tree = pdf_to_etree(assmt_pdf.filepath, self.xml_dir)
            return self.extract_data(elmt_tree, assmt_pdf)
        except Exception as e:
            logger.error(f"Failed to load PDF {assmt_pdf.filepath} {e}")
            self.failure.append(assmt_pdf)

    def retrieve_text_information(self, payload: PDFPayload,
                                  error_handlers: Dict) -> Dict:
        # sections_text dict is return val
        failed = False
        section_texts = {}

        # begin text processing loop
        for k, v in payload.sections.items():
            try:
                a = accumulate_lines(
                    payload.texts,
                    payload.texts.index(v),
                    lambda x: not re.match(r"^Accept$",
                                           "".join(x.itertext()))
                )
                response_data = ["".join(i.itertext()) for i in a]
                score = find_compliance_level(response_data)
                response_text = find_response(response_data)
                section_texts[k] = {
                    "compliance_level": score,
                    "response": response_text
                }
            except Exception:
                try:
                    resp_data = error_handlers["alt_method"](payload, v)
                    section_texts[k] = resp_data
                except Exception as e:
                    logger.error(
                        "Failed to extract information from PDF {}".format(
                            payload.document.filepath) + f" {e}")
                    self.failure.append(payload.document)
                    failed = True
        # end text processing loop
        if not failed:
            payload.section_data = section_texts
            self.success.append(payload)

    def alt_text_processing(self, payload: PDFPayload, val):
        logger.warning(
            f"Using alt_text_processing for {payload.document.filepath}")
        a = accumulate_lines(
            payload.texts,
            payload.texts.index(val),
            lambda x: not re.match(
                r"^Accept.*$",
                "".join(x.itertext()))
        )
        response_data = ["".join(i.itertext()) for i in a]
        score = find_compliance_level(response_data)
        # alternate method of findind response text
        start = [
            i for i in response_data
            if re.match(r"^R[0-9]{1,2}\..+", i)
        ]
        print(f"start: {start}")
        if len(start) > 1:
            response_text = "".join(response_data)
            raise ValueError(response_text)
        else:
            while True:
                try:
                    start = response_data.index(start[0])
                except IndexError:
                    response_text = "".join(response_data)
                    break
                remaining_text = response_data[start:]
                stop = [
                    i for i in remaining_text
                    if re.match(r"^Reviewer.{1,15}$", i)
                ]
                try:
                    stop = remaining_text.index(stop[0])
                except IndexError:
                    response_text = "".join(response_data)
                    break
                index_offset = len(response_data) - len(remaining_text)
                stop += index_offset
                response_text = "\n".join(response_data[start:stop:])
                return {
                    "compliance_level": score,
                    "response": response_text
                }

    def output_dict(self):
        data = {
            "2017_2019": [
                {
                    "repository_uuid": uuid4().urn,
                    "name": i.document.pdf_repo_name,
                    "website": i.document.website,
                    "pdf_website": i.document.pdf_website,
                    "seal_date": i.document.cert_date,
                    "entries": {
                        remove_trailing_ws(k): v
                        for k, v in i.section_data.items()
                    }
                }
                for i in self.success
            ]
        }
        # check to make sure keys are standardized
        std_keys = reduce(
            lambda x, y: set(
                [i for i in x]
            ).union(
                set([i for i in y])),
            [i["entries"].keys() for i in data["2017_2019"]])
        if len(std_keys) == 16:
            logger.info("Keys successfully standardized")
        else:
            raise ValueError(
                "PDF text serialization generated invalid keys")
        return data

    def write_to_file(self, filepath=None):
        filepath = filepath or self.output_filepath
        data = json.dumps(self.output_dict(), indent=4)
        with open(filepath, "w") as f:
            f.write(data)
        logger.info(f"Extracted PDF data written to {filepath}")

    def report_success(self, report_filepath=None):
        report_filepath = report_filepath or self.success_rpt_fpath
        data = [
            {
                "name": i.document.pdf_repo_name,
                "website": i.document.website,
                "pdf_website": i.document.pdf_website,
                "seal_date": i.document.cert_date
            } for i in self.success
        ]
        with open(report_filepath, "w", newline="") as csvfile:
            fieldnames = ["name", "website", "pdf_website", "seal_date"]
            writer = csv.DictWriter(csvfile,
                                    fieldnames=fieldnames,
                                    quoting=csv.QUOTE_NONNUMERIC)
            writer.writeheader()
            for row in data:
                writer.writerow(row)
        return report_filepath

    def report_failure(self, report_filepath=None):
        report_filepath = report_filepath or self.failure_rpt_fpath
        data = {i.filepath: i.website for i in self.failure}
        data = [
            {
                "filepath": k,
                "website": v
            } for k, v in data.items()
        ]
        with open(report_filepath, "w", newline="") as csvfile:
            fieldnames = ["filepath", "website"]
            writer = csv.DictWriter(csvfile,
                                    fieldnames=fieldnames,
                                    quoting=csv.QUOTE_NONNUMERIC)
            writer.writeheader()
            for row in data:
                writer.writerow(row)
        return report_filepath

    def process_assessments(self, assessments: List[AssessmentPDF]):
        for assmt in assessments:
            payload = self.load_pdf(assmt)
            if payload:
                error_handlers = {"alt_method": self.alt_text_processing}
                self.retrieve_text_information(payload, error_handlers)

    @classmethod
    def run(cls,
            file_manifest_path=None,
            log_file_handler=None,
            xml_dir=None,
            output_filepath=None,
            success_rpt_fpath=None,
            failure_rpt_fpath=None,
            **kwargs):
        inst = cls.make(file_manifest_path=file_manifest_path,
                        log_file_handler=log_file_handler,
                        xml_dir=xml_dir,
                        output_filepath=output_filepath,
                        success_rpt_fpath=success_rpt_fpath,
                        failure_rpt_fpath=failure_rpt_fpath,
                        **kwargs)
        manifest = inst.file_manifest
        assmts = [
            AssessmentPDF(i["filepath"], i["website"])
            for i in manifest["pdf_file_info"]
        ]
        inst.process_assessments(assmts)
        fail_count = len(set([i.filepath for i in inst.failure]))
        logger.info(
            f"total processed: {len(inst.success) + fail_count}")
        logger.info(f"total succeeded: {len(inst.success)}")
        logger.info(f"total failed: {fail_count}")
        inst.write_to_file()
        inst.report_failure()
        inst.report_success()
