import logging
from typing import List

from src.Data.constants import PKG_LOOKUP, DB_CONFIG_MAP
from src.Data.database_config import DatabaseConfig
from src.Data.database_migration import MigrationFactory
import src.Data.text_models as models

logger = logging.getLogger(__name__)

db_cfg = DatabaseConfig.from_env(DB_CONFIG_MAP)
migration = MigrationFactory.make_migration(db_cfg)
tbl = migration.get_tables()


@migration.mysql()
def all_repositories(session):
    repo = tbl.contact_data_repository
    return {
        "repositories_query": session.query(
            repo.id,
            repo.title,
            repo.website).all()
    }


@migration.mysql()
def guideline_packages(session):
    pkgs = tbl.dsa_data_guidelinepackage
    return {
        "guideline_packages": session.query(
            pkgs.id,
            pkgs.name).all()
    }


@migration.mysql()
def guidelines(session):
    query = """
SELECT
    dsa_data_guideline.id,
    dsa_data_order.guidelinepackage_id,
    dsa_data_order.order_number,
    dsa_data_guideline.title
FROM dsa_data_guideline
    INNER JOIN dsa_data_order
    ON dsa_data_guideline.id=dsa_data_order.guideline_id;
"""
    # Note: see documentation for assurance that only
    # guidelines with an order_number are present in
    # assessment_data_entry records.
    guidelines = session.execute(query)
    return {
        "guidelines": guidelines.fetchall()
    }


@migration.mysql()
def non_empty_assessments(session):
    query = """
SELECT
assmts.id,
assmts.repository_id,
assmts.guidelinepackage_id,
assmts.seal_date,
assmts.creation_date
FROM
(SELECT 
    assessment_data_assessment.id,
    assessment_data_assessment.repository_id,
    assessment_data_assessment.guidelinepackage_id,
    assessment_data_assessment.seal_date,
    assessment_data_assessment.creation_date,
    COUNT(assessment_data_entry.id) AS entry_count
    FROM assessment_data_entry
    INNER JOIN assessment_data_assessment
    ON assessment_data_assessment.id = assessment_data_entry.assessment_id
    GROUP BY assessment_data_assessment.id) AS assmts
WHERE assmts.entry_count >= 16;
"""
    assessments = session.execute(query)
    return {
        "assessments": assessments.fetchall()
    }


@migration.mysql()
def assessment_entries(session, assessments):
    entries = tbl.assessment_data_entry
    assmt_ids = [i.id for i in assessments]
    output_entries = {}
    for i in assmt_ids:
        assmt_entries = session.query(
            entries.id,
            entries.assessment_id,
            entries.guideline_id,
            entries.statement_of_compliance_id,
            entries.creation_date,
            entries.comment
        ).filter(
            entries.assessment_id == i
        ).group_by(
            entries.guideline_id
        ).order_by(
            entries.creation_date.desc()
        ).all()
        output_entries[i] = assmt_entries
    return {
        "assessments": assessments,
        "entries": output_entries
    }


@migration.sqlite()
def load_repositories(session, repositories_query: List = None):
    for row in repositories_query:
        repo = models.Repository(dsa_db_id=row.id,
                                 name=row.title,
                                 website=row.website)
        session.add(repo)
    session.commit()


@migration.sqlite()
def load_guideline_packages(session,
                            guideline_packages: List = None):
    for row in guideline_packages:
        pkg = models.GuidelinePackage(dsa_db_id=row.id,
                                      name=row.name)
        session.add(pkg)
    session.commit()


@migration.sqlite()
def load_guidelines(session, guidelines = None):
    for row in guidelines:
        gdln = models.Guideline(
            dsa_db_id=row.id,
            dsa_db_order_num=row.order_number,
            title=row.title,
            guideline_package_id=PKG_LOOKUP[row.guidelinepackage_id],
            dsa_db_guidelinepackage_id=row.guidelinepackage_id
        )
        session.add(gdln)
    session.commit()


@migration.sqlite()
def load_assessments_(session, assessments = None):
    for row in assessments:
        assoc_repo = session.query(
            models.Repository
        ).filter_by(
            dsa_db_id=row.repository_id).first()
        assmt = models.Assessment(
            dsa_db_id=row.id,
            creation_date=row.creation_date,
            seal_date=row.seal_date,
            guideline_package_id=PKG_LOOKUP[row.guidelinepackage_id],
            dsa_db_guidelinepackage_id=row.guidelinepackage_id,
            repository_id=assoc_repo.id,
            dsa_db_repository_id=row.repository_id
        )
        session.add(assmt)
    session.commit()


@migration.sqlite()
def load_assessments(session, assessments, entries):
    for row in assessments:
        assoc_repo = session.query(
            models.Repository
        ).filter_by(
            dsa_db_id=row.repository_id).first()
        assmt = models.Assessment(
            dsa_db_id=row.id,
            creation_date=row.creation_date,
            seal_date=row.seal_date,
            guideline_package_id=PKG_LOOKUP[row.guidelinepackage_id],
            dsa_db_guidelinepackage_id=row.guidelinepackage_id,
            repository_id=assoc_repo.id,
            dsa_db_repository_id=row.repository_id
        )
        session.add(assmt)
        session.flush()

        assmt_entries = entries[row.id]
        print(type(assmt_entries))
        for entry_row in assmt_entries:
            guideline = session.query(
                models.Guideline
            ).filter_by(
                dsa_db_id=entry_row.guideline_id
            ).first()
            entry = models.AssessmentEntry(
                dsa_db_id=entry_row.id,
                compliance_level=entry_row.statement_of_compliance_id,
                response=entry_row.comment,
                dsa_db_assessment_id=entry_row.assessment_id,
                assessment_id=assmt.id,
                dsa_db_guideline_id=entry_row.guideline_id,
                guideline_id=guideline.id
            )
            session.add(entry)

    session.commit()


migration.add_steps(all_repositories,
                    load_repositories,
                    guideline_packages,
                    load_guideline_packages,
                    guidelines,
                    load_guidelines,
                    non_empty_assessments,
                    assessment_entries,
                    load_assessments)
