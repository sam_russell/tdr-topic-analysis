PKG_LOOKUP = {1: 1, 52: 2, 54: 3}

DB_CONFIG_MAP = {
        "user": "MYSQL_USERNAME",
        "password": "MYSQL_PASSWORD",
        "db_name": "MYSQL_DB_NAME",
        "db_host": "MYSQL_DB_HOST",
        "db_port": "MYSQL_DB_PORT"
    }

SECTIONS_LOOKUP = {
    'Mission/Scope': 50,
    'Licenses': 49,
    'Continuity of access': 48,
    'Confidentiality/Ethics': 47,
    'Organizational infrastructure': 46,
    'Expert guidance': 45,
    'Data integrity and authenticity': 44,
    'Appraisal': 43,
    'Documented storage procedures': 42,
    'Preservation plan': 41,
    'Data quality': 40,
    'Workflows': 39,
    'Data discovery and identification': 38,
    'Data reuse': 37,
    'Technical infrastructure': 36,
    'Security': 35
}


KEYS = [
    'Mission/Scope',
    'Licenses',
    'Continuity of access',
    'Confidentiality/Ethics',
    'Organizational infrastructure',
    'Expert guidance',
    'Data integrity and authenticity',
    'Appraisal',
    'Documented storage procedures',
    'Preservation plan',
    'Data quality',
    'Workflows',
    'Data discovery and identification',
    'Data reuse',
    'Technical infrastructure',
    'Security'
]
