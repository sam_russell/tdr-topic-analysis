# -*- coding: utf-8 -*-

from datetime import datetime
import importlib
import json
import logging
import os
from typing import Callable, Dict

import click

from src.Data.bagger import Bagger
from src.Data.database_config import DatabaseConfig
from src.Data.database_migration import MySQLDataLoader, Migration, \
    MySQLManager, SQLiteManager
from src.Data.entity_matcher import EntityMatcher
from src.Data.extract_pdf_text import PDFTextExtractor
from src.Data.pull_raw import FileDataRetrieval
from src.utils import dir_or_create, timestamped

AUTHORS = {
    "Authors": [
        "Devan Donaldson",
        "Sam Russell"
    ],
    "Title": "Trustworthy Data Repository Topic Analysis Raw Data"
}

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


class DatasetBuilder:
    def __init__(self,
                 start_timestamp=None,
                 bagit_dir=None,
                 sqlite_filepath=None,
                 sqldump_filepath=None,
                 pdf_file_manifest=None,
                 state_data_filepath=None,
                 assmt_data_filepath=None,
                 failure_rpt_fpath=None,
                 success_rpt_fpath=None,
                 logs_dir=None,
                 match_success_filepath=None,
                 *args, **kwargs):
        self.start_timestamp = start_timestamp
        self.bagit_dir = bagit_dir
        self.sqlite_filepath = sqlite_filepath
        self.sqldump_filepath = sqldump_filepath
        self.pdf_file_manifest = pdf_file_manifest
        self.state_data_filepath = state_data_filepath
        self.assmt_data_filepath = assmt_data_filepath
        self.failure_rpt_fpath = failure_rpt_fpath
        self.success_rpt_fpath = success_rpt_fpath
        self.match_success_filepath = match_success_filepath
        self.logs_dir = logs_dir
        self.log_files = []

    def get_bagger(self):
        return Bagger(self.bagit_dir, AUTHORS)

    def as_json(self):
        data = {k: v for k, v in self.__dict__.items() if k[0] != "_"}
        data["start_timestamp"] = data["start_timestamp"].strftime('%FT%T')
        return json.dumps(data)

    @classmethod
    def from_json(cls, json_str):
        data = json.loads(json_str)
        data["start_timestamp"] = datetime.fromisoformat(
            data["start_timestamp"])
        return cls(**data)

    @classmethod
    def new_builder(cls):
        start_timestamp = datetime.now()
        bagit_dir = os.path.join(
            "data", "raw", timestamped("downloads", start_timestamp)
        )
        return cls(start_timestamp=start_timestamp,
                   bagit_dir=bagit_dir)

    @classmethod
    def from_file(cls, filepath):
        with open(filepath, "r") as f:
            json_str = f.read()
        inst = cls.from_json(json_str)
        if not inst.state_data_filepath:
            inst.state_data_filepath = filepath
        return inst

    def get_file_handler(self, handler_name: str) -> logging.FileHandler:
        fname = timestamped(handler_name, self.start_timestamp) + ".log"
        fpath = os.path.join(self.logs_dir, fname)
        file_handler = logging.FileHandler(fpath)
        file_handler.setFormatter(logging.Formatter(LOG_FORMAT))
        self.log_files.append(fpath)
        return file_handler

    def save_state(self):
        with open(self.state_data_filepath, "w") as f:
            f.write(self.as_json())


def load_module(module_str):
    module_path = os.path.join(*[i for i in module_str.split(".")])
    module_path = os.path.join(os.getcwd(), module_path)
    module_path += ".py"
    print(module_path)
    spec = importlib.util.spec_from_file_location(
        module_str, module_path)
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)
    return mod


@click.group()
def cli():
    pass


@cli.command()
@click.option("--builder-state-data")
def init_builder(builder_state_data):
    dataset_builder = DatasetBuilder.new_builder()
    with dataset_builder.get_bagger() as bag:
        logs_dir = os.path.join(bag, "logs")
        os.mkdir(logs_dir)
        dataset_builder.logs_dir = logs_dir
    if not builder_state_data:
        builder_state_data = timestamped("builder_state_data",
                                         dataset_builder.start_timestamp)
        builder_state_data += ".json"
    dataset_builder.state_data_filepath = builder_state_data
    dataset_builder.save_state()


@cli.command()
@click.option("--builder-state-data")
@click.option("--test", is_flag=True)
def retrieve_raw_data(builder_state_data, test):
    dataset_builder = DatasetBuilder.from_file(builder_state_data)
    with dataset_builder.get_bagger() as bag:
        sqldump = FileDataRetrieval.get_sql(bag)
        dataset_builder.sqldump_filepath = os.path.join(bag, sqldump)
        pdf_file_manifest_path = os.path.join(bag, "pdf_info.json")
        FileDataRetrieval.get_pdf_files(
            FileDataRetrieval.get_pdf_urls(),
            bag,
            manifest_filepath=pdf_file_manifest_path,
            test=test
        )
        dataset_builder.pdf_file_manifest = pdf_file_manifest_path
    click.echo(f"BUILDER: {dataset_builder.as_json()}")
    dataset_builder.save_state()


@cli.command()
@click.option("--builder-state-data")
def load_dsa_sqldump(builder_state_data):
    dataset_builder = DatasetBuilder.from_file(builder_state_data)
    loader = MySQLDataLoader.from_env()
    loader.sqldump_filepath = dataset_builder.sqldump_filepath
    loader.make_database()


@cli.command()
@click.option("--builder-state-data")
@click.option("--target-sqlite-filepath")
def migrate_dsa_database(builder_state_data,
                         target_sqlite_filepath):
    dataset_builder = DatasetBuilder.from_file(builder_state_data)
    if not target_sqlite_filepath:
        sqlite_fname = timestamped(
            "data_repository_assmts",
            dataset_builder.start_timestamp
        )
        sqlite_fname += ".db"
        target_sqlite_filepath = os.path.join("data",
                                              "interim",
                                              sqlite_fname)
    dataset_builder.sqlite_filepath = target_sqlite_filepath
    os.environ["SQLITE_DB_FILEPATH"] = target_sqlite_filepath
    migration = load_module("src.Data.migration_script").migration
    migration.run()
    dataset_builder.save_state()


@cli.command()
@click.option("--builder-state-data")
@click.option("--pdf-file-manifest")
def extract_pdf_text(builder_state_data, pdf_file_manifest):
    dataset_builder = DatasetBuilder.from_file(builder_state_data)
    if not pdf_file_manifest:
        pdf_file_manifest = dataset_builder.pdf_file_manifest
    with dataset_builder.get_bagger() as bag:
        xml_dir = os.path.join(bag, "assmt_xml")
        if not os.path.exists(xml_dir):
            os.mkdir(xml_dir)
        output_filepath = os.path.join(
            bag, "CTS_2017_2019_assessments.json")
        success_rpt_fpath = os.path.join(bag, "success.csv")
        failure_rpt_fpath = os.path.join(bag, "failure.csv")
        file_handler = dataset_builder.get_file_handler("text_extraction")
        PDFTextExtractor.run(
            file_manifest_path=pdf_file_manifest,
            xml_dir=xml_dir,
            output_filepath=output_filepath,
            success_rpt_fpath=success_rpt_fpath,
            failure_rpt_fpath=failure_rpt_fpath,
            log_file_handler=file_handler
        )
    dataset_builder.assmt_data_filepath = output_filepath
    dataset_builder.success_rpt_fpath = success_rpt_fpath
    dataset_builder.failure_rpt_fpath = failure_rpt_fpath
    dataset_builder.save_state()


@cli.command()
@click.option("--builder-state-data")
@click.option("--failure-report")
def match_entities(builder_state_data, failure_report):
    dataset_builder = DatasetBuilder.from_file(builder_state_data)
    failure_rpt = failure_report or dataset_builder.failure_rpt_fpath
    with dataset_builder.get_bagger() as bag:
        success_match_filepath = os.path.join(bag, "match_success.csv")
        failure_match_filepath = os.path.join(bag, "match_failure.csv")
        file_handler = dataset_builder.get_file_handler("entity_matching")
        matcher = EntityMatcher.make(
            json_filepath=dataset_builder.assmt_data_filepath,
            sqlite_filepath=dataset_builder.sqlite_filepath,
            success_match_filepath=success_match_filepath,
            failure_match_filepath=failure_match_filepath,
            failure_report_filepath=failure_rpt,
            file_handler=file_handler
            )
        matcher.build_migration()
        matcher.generate_matching_tables()
    dataset_builder.match_success_filepath = success_match_filepath
    dataset_builder.save_state()


@cli.command()
@click.option("--builder-state-data")
@click.option("--success-match-report")
def process_matched_entities(builder_state_data, success_match_report):
    dataset_builder = DatasetBuilder.from_file(builder_state_data)
    success_match_report = success_match_report or \
        dataset_builder.match_success_filepath
    with dataset_builder.get_bagger() as bag:
        failure_match_filepath = os.path.join(bag, "match_failure.csv")
        matcher = EntityMatcher.make(
            json_filepath=dataset_builder.assmt_data_filepath,
            sqlite_filepath=dataset_builder.sqlite_filepath,
            success_match_filepath=success_match_report,
            failure_match_filepath=failure_match_filepath,
            failure_report_filepath=dataset_builder.failure_rpt_fpath,
            file_handler=dataset_builder.get_file_handler("entity_matching")
            )
        matcher.build_matched_entities_migration()
        matcher.migration.run()


cli.add_command(init_builder)
cli.add_command(retrieve_raw_data)
cli.add_command(migrate_dsa_database)
cli.add_command(load_dsa_sqldump)
cli.add_command(extract_pdf_text)
cli.add_command(match_entities)
cli.add_command(process_matched_entities)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    #project_dir = Path(__file__).resolve().parents[2]

    cli()
