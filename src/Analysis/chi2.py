import os

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import scipy
from scipy import cluster

from src.Models.predict_model import NLP


DATA = "/home/sam/Desktop/similarity_data_2020-11-10T15:57:20.csv"


def bin_similarities(df, num_bins):
    """Uses pd.cut to bin and count frequency of similarity ranges"""
    bins = pd.cut(df["similarity"], num_bins)
    bins_count = {
        f"{i.right}_{i.left}": len([j for j in bins if j.right == i.right])
        for i in bins
    }
    return bins_count


# Scores Histogram Title
# Change in Cumulative Compliance Score after Re-Certification
def scores_hist(df, num_bins=20):
    h = plt.hist(
        x=df["score_change"],
        bins=num_bins,
        color="gold"
    )
    plt.xlabel("Change in Cumulative Compliance Score after Re-Certification")
    plt.ylabel("Frequency")
    return h


def similarities_hist(df, num_bins=20):
    h = plt.hist(x=df["similarity"], bins=num_bins)
    plt.xlabel("Assessment Document Similarity Between Re-Certifications")
    plt.ylabel("Frequency")
    return h


def times_certified(df, n):
    re_cert = [
        j for j in df["repository_id"]
        if [i for i in df["repository_id"]].count(j) == n
    ]
    return df[df["repository_id"].isin(re_cert)]


def plot_by_times_cert(df, output_dir):
    times_cert = {"1": 1, "2": 3}
    for k, v in times_cert.items():
        similarities_hist(times_certified(df, v))
        plt.title(f"Re-certified {k} times")
        fpath_sim = os.path.join(
            output_dir,
            f"similarities_hist_x{k}.png"
            )
        plt.ylim(0, 12)
        plt.savefig(fpath_sim)
        plt.close()

        scores_hist(times_certified(df, v))
        plt.title(f"Re-certified {k} times")
        fpath_score = os.path.join(
            output_dir,
            f"scores_hist_x{k}.png"
           )
        plt.ylim(0, 12)
        plt.savefig(fpath_score)
        plt.close()


def score_similarity_scatter(df, output_dir):
    plt.scatter(x=df["similarity"], y=df["score_change"])
    plt.xlabel("Document similarity")
    plt.ylabel("Change in cumulative score")


def cumulative_score_mean_similarity(df, output_dir):
    pass


# Assessment Document Similarity Between Re-Certifications


def group_by_score(df):
    assmts = df[["repository_id",
                 "repository_name",
                 "similarity"]]
    scores = df["score_change"].unique()
    for idx, row in df.iterrows():
        pass


def make_cluster_array(df):
    return [
        {
            "row_idx": idx,
            "terms_added_lexemes": [
                NLP.vocab[i]  # (NLP.vocab[i].norm, NLP.vocab[i].vector_norm)
                for i in row.terms_added.split(",")
            ]
        }
        for idx, row in df.iterrows()
        ]


def view_clustered_terms(clusters, idx,
                         terms_key="terms_added_lexemes",
                         num_clusters=5):
    data = cluster.vq.whiten(
        np.array(
            [
                (i.norm, i.vector_norm)
                for i in clusters[idx][terms_key]
                if i.has_vector
            ]
        )
    )

    centroids, magnitude = cluster.vq.kmeans(data, num_clusters)

    sorted_clusters_20 = []

    for centroid in centroids:
        selection = sorted(
            {
                lexeme.text: abs(lexeme.vector_norm - centroid[1])
                for lexeme in clusters[idx][terms_key]
            }.items(),
            key=lambda x: x[1]
        )
        sorted_clusters_20.append(selection[:20])

    return sorted_clusters_20
