from collections import namedtuple
from functools import reduce, partial, partialmethod
import json
import logging
import math
from typing import Dict, List, Callable

from adjustText import adjust_text
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn.decomposition import PCA, LatentDirichletAllocation
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import spacy

from src.Data.database_migration import SQLiteManager
from src.Data.text_models import AssessmentEntry, Assessment, Repository, \
    Guideline, GuidelinePackage
from src.Models.build_model import AssessmentDocument, TFIDF, \
    TextCleaner, TextProcessor, DB, NLP, filter_tokens
from src import utils

ADDED = "added"
REMOVED = "removed"

SENTENCE = "sentence"
DOCUMENT = "document"
TOKEN = "token"
ENTRY = "entry"

COLORS = ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a',
          '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94',
          '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d',
          '#17becf', '#9edae5']

TermsChanged = namedtuple("TermsChanged", ["added", "removed"])
#class TermsChanged:
#    def __init__(self, added, removed):
#        """Args may be List or Set!"""
#        self.added = added
#        self.removed = removed


class Term:

    def __init__(
            self,
            token,
            assmt_doc: AssessmentDocument,
            tfidf: TFIDF
    ):
        self.token = token
        self.lemmatized = token.lemma_
        try:
            self.lemmatized_id = token.lemma
        except AttributeError as e:
            print(e)
            self.lemmatized_id = 0
            pass
        self.doc = assmt_doc
        self.weight = tfidf.get_tfidf_weights(
            assmt_doc).get(token.lemma_)

    @classmethod
    def get_terms_list(
            cls,
            assmt_doc,
            tfidf: TFIDF,
            filter_func=filter_tokens
    ):
        doc = assmt_doc.doc
        if filter_func:
            doc = filter_func(doc)
        return [cls(t, assmt_doc, tfidf) for t in doc]


def make_model(merge_noun_chunks=False):
    tc = TextCleaner(DB)
    tp = TextProcessor.make(tc, NLP, merge_noun_chunks)
    
    def make_term(text_processor, t, d):
        return Term(t, d, text_processor.tfidf)
    
    tp.make_term = partial(make_term, tp)
    return tp


def find_next_report(doc: AssessmentDocument, docs: Dict[int, AssessmentDocument]):
    next_rpt = filter(
        lambda x: doc.assmt.repository.id == x.assmt.repository.id
        and x.assmt.guideline_package.id == doc.assmt.guideline_package.id + 1,
        docs.values()
    )
    next_rpt = list(next_rpt)
    if not next_rpt:
        return None
    else:
        if len(next_rpt) > 1:
            raise ValueError("Data anomaly in number of reports found")
        return next_rpt[0]


def terms_difference(doc_a, doc_b, tfidf):
    former = Term.get_terms_list(doc_a, tfidf)
    latter = Term.get_terms_list(doc_b, tfidf)
    added = {t.lemmatized_id for t in latter}.difference(
        {t.lemmatized_id for t in former}
    )
    removed = {t.lemmatized_id for t in former}.difference(
        {t.lemmatized_id for t in latter}
    )
    return TermsChanged(
        added=[t for t in latter if t.lemmatized_id in added],
        removed=[t for t in former if t.lemmatized_id in removed]
        )


def get_comparison_name(doc_a, doc_b):
    former = doc_a.assmt.guideline_package.name
    latter = doc_b.assmt.guideline_package.name
    return f"{former}_{latter}"


def get_document_similarity(doc_a, doc_b):
    return doc_a.doc.similarity(doc_b.doc)


class Comparison:
    """Represents the comparison of two subsequent AssessmentDocument objects
belonging to the same repository"""

    def __init__(self, doc_a, doc_b):
        self.a = doc_a
        self.b = doc_b

    @classmethod
    def make(cls, doc, docs):
        doc_b = find_next_report(doc, docs)
        if not doc_b:
            return None
        return cls(doc, doc_b)

    def get_terms_diff(self):
        return terms_difference(self.a, self.b, self.a.processor.tfidf)


def cumulative_score(session, assmt):
    entries = session.query(
        AssessmentEntry
    ).filter_by(
        assessment_id=assmt.id
    ).all()
    #print(len([
    #    i.compliance_level for i in entries
    #    if i.guideline_id not in [51, 34, 33]
    #]))
    return sum([
        i.compliance_level for i in entries
        if i.guideline_id not in [51, 34, 33]
    ])
    

class TableRow:
    """Represents the comparison of two subsequent AssessmentDocument objects
belonging to the same repository"""

    def __init__(self, comp: Comparison, term: Term, term_change_type: str):
        self.repository_id_a = comp.a.assmt.repository.id
        self.repository_id_b = comp.b.assmt.repository.id
        self.assessment_id_a = comp.a.assmt.id
        self.assessment_id_b = comp.b.assmt.id
        self.guideline_package_id_a = comp.a.assmt.guideline_package.id
        self.guideline_package_id_b = comp.b.assmt.guideline_package.id
        self.guideline_package_name_a = comp.a.assmt.guideline_package.name
        self.guideline_package_name_b = comp.b.assmt.guideline_package.name
        self.repository_name = comp.a.assmt.repository.name
        score_a, score_b = [
            cumulative_score(
                rpt.processor.text_cleaner.session,
                rpt.assmt
            ) for rpt in [comp.a, comp.b]
        ]
        self.cumulative_score_a = score_a
        self.cumulative_score_b = score_b
        self.cumulative_score_change = score_b - score_a
        self.similarity_score = get_document_similarity(comp.a, comp.b)
        self.document_vector_norm_a = comp.a.doc.vector_norm
        self.document_vector_norm_b = comp.b.doc.vector_norm
        self.term_changed = term.lemmatized
        self.term_index = term.token.i
        self.change_type = term_change_type
        self.term_vector_norm = term.token.vector_norm
        self.term_weight = term.weight or 0
        self.term_usage = term.token.sent.text
        self.term_usage_vector_norm = term.token.sent.vector_norm

    @classmethod
    def from_comparison(cls, comp: Comparison):
        if not comp:
            return []
        diff = comp.get_terms_diff()
        if diff:
            added = [cls(comp, term, ADDED) for term in diff.added]
            removed = [cls(comp, term, REMOVED) for term in diff.removed]
            return added + removed
        else:
            return []
    


def comp_to_df(comp: Comparison):
    tbl_rows = TableRow.from_comparison(comp)
    if tbl_rows:
        return pd.DataFrame([r.__dict__ for r in tbl_rows])
    else:
        return None


def process_dataset(text_processor):
    df = None
    progress = 0
    for key, doc in text_processor.docs.items():
        compared = Comparison.make(doc, text_processor.docs)
        print(f"progress: {progress} ... ")
        if progress == 0:
            df = comp_to_df(compared)
            progress += 1
        else:
            new_data = comp_to_df(compared)
            progress += 1
            if new_data is not None:
                df = df.append(new_data)
            else:
                continue
    return df


def find_top_terms(df, change_type):
    df = df[(df.change_type == change_type) & (df.term_weight > 0)]
    qtl = df.term_weight.quantile([0, 0.25, 0.50, 1])
    qtl = [i for i in qtl]
    for i in qtl:
        print(i)
    low = df[(df.term_weight >= qtl[0]) & (df.term_weight <= qtl[1])]
    mid = df[(df.term_weight >= qtl[1]) & (df.term_weight <= qtl[2])]
    high = df[(df.term_weight >= qtl[2]) & (df.term_weight <= qtl[3])]
    return [low, mid, high]


def handle_top_terms(tt_lst, df=None):
    result = []
    for i in tt_lst:
        vc = i.term_changed.value_counts()[:20]
        data = dict(zip(vc.index, [j for j in vc]))
        for k, v in data.items():
            term_data = i[i.term_changed == k]
            sentences = {
                (
                    row.term_usage_vector_norm
                    - abs(term_data.term_usage_vector_norm.mean())
                ): row.term_usage
                for idx, row in term_data.iterrows()
            }
            sent_sorted = sorted(sentences.keys())
            sent_sorted = sent_sorted[:3] + sent_sorted[-1:]
            # sent = sentences[sorted(sentences.keys())[0]]
            # repository = term_data[term_data.term_usage == sent]
            for sent in sent_sorted:
                sent = sentences[sent]
                repository = term_data[term_data.term_usage == sent]
                term_idx = repository.iloc[0].term_index
                repo_id = repository.iloc[0].repository_id_a
                repo_name = repository.iloc[0].repository_name
                gd_a = repository.iloc[0].guideline_package_name_a
                gd_b = repository.iloc[0].guideline_package_name_b
                result.append(
                    {
                        "term": k,
                        "term_count": v,
                        "mean_sentence_vec_norm": term_data.term_usage_vector_norm.mean(),
                        "mean_score_change": term_data.cumulative_score_change.mean(),
                        "mean_compliance_score": term_data.cumulative_score_b.mean(),
                        "representative_sentence": sent,
                        "sentence_vector_norm": repository.iloc[0].term_usage_vector_norm,
                        "repository_id": repo_id,
                        "repository_name": repo_name,
                        "term_index": term_idx,
                        "doc_a_gdl": gd_a,
                        "doc_b_gdl": gd_b,
                        "doc_a_idx": repository.iloc[0].assessment_id_a,
                        "doc_b_idx": repository.iloc[0].assessment_id_b
                    }
                )
    return result



def handle_top_terms_(tt_lst):
    result = []
    for i in tt_lst:
        vc = i.term_changed.value_counts()[:20]
        data = dict(zip(vc.index, [j for j in vc]))
        for k, v in data.items():
            term_data = i[i.term_changed == k]
            sentences = {
                (
                    row.term_usage_vector_norm
                    - abs(term_data.term_usage_vector_norm.mean())
                ): row.term_usage
                for idx, row in term_data.iterrows()
            }
            sent_sorted = sorted(sentences.keys())
            sent_sorted[:3] + sent_sorted[-1:]
            # sent = sentences[sorted(sentences.keys())[0]]
            # repository = term_data[term_data.term_usage == sent]
            repository = [term_data[term_data.term_usage == sent]
                          for sent in sent_sorted]
            term_idx = repository.iloc[0].term_index
            repo_id = repository.iloc[0].repository_id_a
            repo_name = repository.iloc[0].repository_name
            gd_a = repository.iloc[0].guideline_package_name_a
            gd_b = repository.iloc[0].guideline_package_name_b
            result.append(
                {
                    "term": k,
                    "term_count": v,
                    "mean_sentence_vec_norm": term_data.term_usage_vector_norm.mean(),
                    "mean_score_change": term_data.cumulative_score_change.mean(),
                    "mean_compliance_score": term_data.cumulative_score_b.mean(),
                    "representative_sentence": sent,
                    "repository_id": repo_id,
                    "repository_name": repo_name,
                    "term_index": term_idx,
                    "doc_a_gdl": gd_a,
                    "doc_b_gdl": gd_b,
                    "doc_a_idx": repository.iloc[0].assessment_id_a,
                    "doc_b_idx": repository.iloc[0].assessment_id_b
                }
            )
    return result



def top_terms_removed(df, to_file=None):
    tt = find_top_terms(df, REMOVED)
    r = pd.DataFrame(handle_top_terms(tt))
    if to_file:
        r.to_csv(to_file, index=False)
    return r


def top_terms_added(df, to_file=None):
    tt = find_top_terms(df, ADDED)
    r = pd.DataFrame(handle_top_terms(tt))
    if to_file:
        r.to_csv(to_file, index=False)
    return r


def get_top_term_tokens(text_processor, df, change_type):
    result = []
    # TODO: change `x.doc_...idx` to something that actually matches
    # the other keys such as those used in the big dataframe.
    if change_type == REMOVED:
        get_doc_idx = lambda x: x.doc_a_idx
    elif change_type == ADDED:
        get_doc_idx = lambda x: x.doc_b_idx
    for idx, row in df.iterrows():
        doc_idx = get_doc_idx(row)
        token_idx = row.term_index
        result.append(text_processor.docs[doc_idx].doc[token_idx])
    return result


def top_term_pca(top_term_tokens: List):
    tt_vecs = np.array([t.vector for t in top_term_tokens])
    return PCA(n_components=2).fit(tt_vecs)


def top_terms_plot(text_processor, terms_df, change_type):
    if change_type == ADDED:
        tokens = get_top_term_tokens(
            text_processor, terms_df, change_type)
    elif change_type == REMOVED:
        tokens = get_top_term_tokens(text_processor, terms_df, change_type)
    term_vecs = np.array([t.vector for t in tokens])
    pca = PCA(n_components=2).fit(term_vecs)
    dcmp_vals = pca.fit_transform(term_vecs)
    xy = dcmp_vals.transpose()
    plt.scatter(xy[0], xy[1])
    for i in range(len(dcmp_vals)):
        text = tokens[i].text
        plt.annotate(text, dcmp_vals[i])
    plt.show()
    return dcmp_vals


def rand_jitter(arr):
    stdev = .01 * (max(arr) - min(arr))
    return arr + np.random.randn(len(arr)) * stdev


def top_terms_plot_kmeans(text_processor, terms_df, change_type, num_clusters=8,
                          df2=None):
    if change_type == ADDED:
        tokens = get_top_term_tokens(
            text_processor, terms_df, change_type)
        fmt_arg = "Added"
    elif change_type == REMOVED:
        tokens = get_top_term_tokens(text_processor, terms_df, change_type)
        fmt_arg = "Removed"
    elif change_type == None:
        tokens_added = get_top_term_tokens(text_processor, terms_df, ADDED)
        tokens_rmv = get_top_term_tokens(text_processor, df2, REMOVED)
        tokens = tokens_added + tokens_rmv
        fmt_arg = "Added or Removed"
    term_vecs = np.array([t.vector for t in tokens])
    pca = PCA(n_components=2).fit(term_vecs)
    dcmp_vals = pca.fit_transform(term_vecs)
    xy = dcmp_vals.transpose()
    kmeans = KMeans(n_clusters=num_clusters).fit(dcmp_vals)
    colors = COLORS
    prediction = kmeans.fit_predict(dcmp_vals)
    plt.scatter(xy[0], xy[1], c=[colors[i]
                                 for i in prediction])                                                    
    # for i in range(len(dcmp_vals)):
    #     text = tokens[i].text
    #     plt.annotate(text, dcmp_vals[i])
    texts = [
        plt.annotate(tokens[i].text, dcmp_vals[i])
        for i in range(len(dcmp_vals))
    ]
    #adjust_text(texts, only_move={'texts': 'xy'})#, arrowprops=dict(arrowstyle="->", color='r', lw=0.5))
    #adjust_text(texts, only_move={'texts': 'xy'})
    plt.xlabel("PCA1")
    plt.ylabel("PCA2")
    plt.title(f"K-Means Clustering of Principal Component Analysis of 60 Most Frequently {fmt_arg} Terms")
    plt.show()
    return dcmp_vals


def scores_hist(df, num_bins=20):
    h = plt.hist(
        x=df["score_change"],
        bins=num_bins,
        color="gold"
    )
    plt.xlabel("Change in Cumulative Compliance Score after Recertification")
    plt.ylabel("Frequency")
    return plt.show


def similarities_hist(df, num_bins=20):
    h = plt.hist(x=df["similarity_score"], bins=num_bins)
    #mu = h[0].mean()
    #print(mu)
    #sigma = h[0].std()
    #print(sigma)
    #plt.close()
    #print(mu)
    #fig, ax = plt.subplots()
    #n, bins, patches = ax.hist(x=df["similarity_score"], bins=num_bins)
    #y = ((1 / (np.sqrt(2 * np.pi) * sigma)) *
    #     np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
    #print(y)
    #ax.plot(bins, y, "--")
    plt.xlabel("Assessment Document Similarity Between Recertifications")
    plt.ylabel("Frequency")
    return plt.show


def cumulative_score_df(session, assmt_id):
    entries = session.query(
        AssessmentEntry
    ).filter_by(
        assessment_id=assmt_id
    ).all()
    return sum([
        i.compliance_level for i in entries
        if i.guideline_id not in [51, 34, 33]
    ])


# fake data:
def heatmap_(session, df):
    a = df.assessment_id_a.unique()
    b = df.assessment_id_b.unique()
    
    def cumulative_score_df(session, assmt_id):
        entries = session.query(
            AssessmentEntry
        ).filter_by(
            assessment_id=assmt_id
        ).all()
        return sum([
            i.compliance_level for i in entries
            if i.guideline_id not in [51, 34, 33]
        ])

    first_scores = np.array([cumulative_score_df(session, int(i)) for i in a])
    second_scores = np.array([cumulative_score_df(session, int(i)) for i in b])
    score_changes = [i[0] - i[1] for i in zip(second_scores, first_scores)]
    print(len(score_changes))
    plt.hist2d(
        np.array(score_changes),
        np.array(second_scores), (64, 64), cmap=plt.cm.jet)
    plt.colorbar()
    plt.show()



def find_match(doc_a, period_docs):
    return (doc_a,
            *[i for i in period_docs
              if i.assmt.repository.id == doc_a.assmt.repository.id])



def longitudinal(text_processor, pkgs=[1, 2, 3]):
    result = []
    tp = text_processor
    # first, second, outer
    for pkg in pkgs[1:]:
        other = pkg - 1
        earlier_assmts = [a for a in tp.docs.values() if
                          a.assmt.guideline_package.id == other]
        assmts = [a for a in tp.docs.values()
                  if a.assmt.guideline_package.id == pkg]
        pairs = []
        for a in assmts:
            for e in earlier_assmts:
                if a.assmt.repository.id == e.assmt.repository.id:
                    pairs.append((a, e))
        result.append(pairs)
    pairs = []
    outer = [a for a in tp.docs.values() if
             a.assmt.guideline_package.id == pkgs[-1]]
    first = [a for a in tp.docs.values() if
             a.assmt.guideline_package.id == pkgs[0]]
    for a in outer:
        for e in first:
            if a.assmt.repository.id == e.assmt.repository.id:
                pairs.append((a, e))
    result.append(pairs)
    return result


def longitudinal_data(text_processor, pkgs=[1, 2, 3]):
    session = text_processor.text_cleaner.session
    lgt_lists = longitudinal(text_processor, pkgs)
    extra = []
    result = []
    fixed = False
    fixed2 = False
    for lst in lgt_lists:
        for pair in lst:
            prd_a = f"{pair[0].assmt.guideline_package.name}_"
            prd = prd_a + str(pair[1].assmt.guideline_package.name)
            score_a = cumulative_score_df(session, pair[1].assmt.id)
            score_b = cumulative_score_df(session, pair[0].assmt.id)
            if pair[0].assmt.id == 120 and not fixed:
                print("ok it's b")
                score_b = 64
                fixed = True
                print(score_a)
                print(score_b)
            if pair[0].assmt.id == 133:
                if fixed2:
                    continue
                else:
                    fixed2 = True
            extra.append(pair[0].assmt.id)
            result.append({
                "period": prd,
                "cumulative_score_a": score_a,
                "cumulative_score_b": score_b,
                "score_change": score_b - score_a,
                "similarity_score": pair[0].doc.similarity(pair[1].doc)
            })
    return pd.DataFrame(result)


def longitudinal_heatmap(lgtdf):
    plt.hist2d(
        lgtdf.score_change,
        lgtdf.cumulative_score_b,
        (64, 64),
        cmap=plt.cm.jet
    )
    plt.colorbar()
    plt.xlabel("Change in Cumulative Compliance Score after Recertification")
    plt.ylabel("Cumulative Compliance Score")
    return plt.show


def get_ents(text_processor, doc, ignore={'DATE', 'ORDINAL', 'CARDINAL'}):
    for i in doc.doc.ents:
        print(f"{set([t.ent_type_ for t in i])} ... {i}")
       
    return [
        text_processor.make_term(i, doc)
        for i in doc.doc.ents if not bool(
                set([t.ent_type_ for t in i]).intersection(ignore)
        )
    ]

def compare_ents(doc_a: List, doc_b: List, get_fn: Callable = None):
    get = get_fn or get_ents
    a = get(doc_a)
    b = get(doc_b)
    return {s.lemmatized for s in a}, {s.lemmatized for s in b}


def doc_to_alist(doc):
    """converts spacy doc to association list using lemma as keys"""
    result = []
    for t in doc:
        result.append((t.i, t.lemma))
    return result


def no_entities(doc) -> str:
    tokens = [i for i in doc if not i.ent_type]
    return " ".join([i.lemma_ for i in filter_tokens(tokens)])


def make_tfidf(docs):
    return TfidfVectorizer([no_entities(d.doc) for d in docs], max_df=0.95,
                           min_df=2)


def make_lda():
    return LatentDirichletAllocation(n_components=20,
                                     doc_topic_prior=0.00001,
                                     topic_word_prior=0.02)
    
#     for pkg in pkgs[:-1]:
#         yr = [a for a in tp.docs.values()
#               if a.assmt.guideline_package.id == pkg]
#         print(len(yr))
#         for a in yr:
#             doc_b = [
#                 n for n in tp.docs.values()
#                 if n.assmt.guideline_package.id == pkg + 1 or
#                 n.assmt.guideline_package.id == pkg + len(pkgs[:-1])
#             ]
#             doc_b = [n for n in doc_b
#                      if n.assmt.repository.id == a.assmt.repository.id]
#             if doc_b:
#                 for b in doc_b:
#                     try:
#                         score_a = cumulative_score_df(
#                             tp.text_cleaner.session, a.assmt.id)
#                         score_b = cumulative_score_df(
#                             tp.text_cleaner.session, b.assmt.id)
#                     except Exception as e:
#                         print(e)
#                         return b
#                     data = {
#                         "period": f"{a.assmt.guideline_package.name}_{b.assmt.guideline_package.name}",
#                         "cumulative_score_a": score_a,
#                         "cumulative_score_b": score_b,
#                         "score_change": score_b - score_a,
#                         "similarity_score": a.doc.similarity(b.doc)
#                     }
#                 result.append(data)
#     return result
# 

def docs_to_csr(tp):
    indptr = [0]
    indices = []
    data = []
    vocabulary = {}
    for d in tp.docs.values():
        for term in filter_tokens(d.doc):
            index = vocabulary.setdefault(term.lemma_, len(vocabulary))
            indices.append(index)
            data.append(1)
    indptr.append(len(indices))
    return (csr_matrix((data, indices, indptr), dtype=int), vocabulary)


def make_term_count_matrix(text_processor):
    return CountVectorizer(
        text_processor.docs.values(),
        analyzer=lambda x: [t.lemma_ for t in filter_tokens(x.doc)]
    )


def make_tfidf_matrix(text_processor):
    return TfidfVectorizer(
        text_processor.docs.values(),
        analyzer=lambda x: [t.lemma_ for t in filter_tokens(x.doc)],
        max_df=0.75,
        min_df=10)


def find_topic_dist(lda: LatentDirichletAllocation,
                    vectorizer: CountVectorizer,
                    text: List[spacy.tokens.token.Token]):
    """Transforms iterable of SpaCy tokens to
    distribution of topics using LDA."""
    term_matrix = vectorizer.transform([text])
    (topic_dist, *rest) = lda.transform(term_matrix)
    topic_dist = dict(enumerate(topic_dist))
    return dict(sorted(
        topic_dist.items(),
        key=lambda x: x[1],
        reverse=True))


def document_topics(lda: LatentDirichletAllocation,
                    vectorizer: CountVectorizer,
                    text: spacy.tokens.doc.Doc,
                    granularity: str = SENTENCE,
                    text_processor: TextProcessor = None):
    """Applies `find_topic_dist` at a certain level of granularity"""
    if granularity == SENTENCE:
        sentences = list(text.sents)
        return [
            (s, find_topic_dist(lda, vectorizer, filter_tokens(s)))
            for s in sentences
        ]
    elif granularity == TOKEN:
        return [
            (t, find_topic_dist(lda, vectorizer, [t]))
            for t in text
        ]
    elif granularity == DOCUMENT:
        return [
            (text, find_topic_dist(lda, vectorizer, text))
        ]
    elif granularity == ENTRY:
        # argument for `text` must be a doc in this case
        return [
            (e, find_topic_dist(lda, vectorizer, e))
            for e in text_processor.get_doc_entries(text)
        ]


def terms_changed_lda(lda: LatentDirichletAllocation,
                      vectorizer: CountVectorizer,
                      df: pd.DataFrame,
                      change_type: str = None,
                      granularity: str = SENTENCE,
                      text_processor: TextProcessor = None,
                      n_topic_ranks: int = 3):
    result = []
    if change_type == ADDED:
        doc_idx_col = "doc_b_idx"
    else:
        doc_idx_col = "doc_a_idx"
    for idx, row in df.iterrows():
        assmt_doc = text_processor.docs[row[doc_idx_col]]
        doc = assmt_doc.doc
        term = doc[row.term_index]
        if granularity == SENTENCE:
            t_dist = find_topic_dist(lda, vectorizer, term.sent)
        elif granularity == TOKEN:
            t_dist = find_topic_dist(lda, vectorizer, [term])
        else:
            raise ValueError("granularity must be SENTENCE or TOKEN")
        row_data = {}
        topics = list(t_dist.items())
        for i in range(n_topic_ranks):
            topic_rank_label = f"topic_{i+1}"
            topic_score_label = f"{topic_rank_label}_score" 
            row_data[topic_rank_label] = topics[i][0]
            row_data[topic_score_label] = topics[i][1]
        result.append(row_data)
    return result


def all_changes_append_topics(lda: LatentDirichletAllocation,
                              vectorizer: CountVectorizer,
                              df: pd.DataFrame,
                              text_processor: TextProcessor,
                              n_topic_ranks: int = 3):
    result = []
    for idx, row in df.iterrows():
        if row.change_type == 'added':
            doc_idx = row.assessment_id_b
        elif row.change_type == 'removed':
            doc_idx = row.assessment_id_a
        term_idx = row.term_index
        doc = text_processor.docs[doc_idx].doc
        sentence = doc[term_idx].sent
        #topic_dist = list(find_topic_dist(lda, vectorizer, sentence).keys())
        topic_dist = find_topic_dist(lda, vectorizer, sentence)
        first, second, third, *rest = topic_dist.items()
        #topic_cols = list(topic_dist.keys())[:n_topic_ranks]
        #topic_ranks = [f"topic_rank_{i+1}" for i in range(n_topic_ranks)]
        #result.append({
        #    "sentence": sentence.text,
        #    "topic_rank_1": topic_dist[0],
        #    "topic_rank_2": topic_dist[1],
        #    "topic_rank_3": topic_dist[2]
        #})
        result.append({
            "topic_rank_1": first[0],
            "topic_rank_1_score": first[1],
            "topic_rank_2": second[0],
            "topic_rank_2_score": second[1],
            "topic_rank_3": third[0],
            "topic_rank_3_score": third[1]
        })
    return pd.DataFrame(result)
        #result.append(dict(zip(topic_ranks, topic_cols)))
#    return df.join(pd.DataFrame(result))

        


def fit_lda_model(text_processor, term_matrix="count",
                  n_topics=20, alpha=None, beta=None):
    lda = LatentDirichletAllocation(n_components=n_topics,
                                    doc_topic_prior=alpha,
                                    topic_word_prior=beta)
    if term_matrix == "count":
        lda.fit(text_processor.tfidf.term_count_data_csr)
    elif term_matrix == "tfidf":
        lda.fit(text_processor.tfidf.tfidf_data_csr)
    else:
        raise ValueError("term_matrix must be 'count' or 'tfidf'.")
    return lda


def show_lda_topic_terms(lda: LatentDirichletAllocation,
                         vectorizer,
                         n_terms: int = 20):
    return [
        [
            vectorizer.get_feature_names()[i]
            for i in t.argsort()[:-(n_terms + 1):-1]
        ]
        for t in lda.components_
    ]


def lda_topics_df(lda, vectorizer, n_terms=20):
    topic_terms = show_lda_topic_terms(lda, vectorizer, n_terms)
    formatted = {
        f"topic_{k}": v
        for k, v in dict(enumerate(topic_terms)).items()
    }
    return pd.DataFrame(formatted)


def terms_changed_append_topics(lda: LatentDirichletAllocation,
                                vectorizer: CountVectorizer,
                                df: pd.DataFrame,
                                change_type: str = None,
                                granularity: str = SENTENCE,
                                text_processor: TextProcessor = None,
                                n_topic_ranks: int = 3):
    if change_type == ADDED:
        term_data = top_terms_added(df)
    elif change_type == REMOVED:
        term_data = top_terms_removed(df)
    elif change_type == 0:
        change_type = ADDED
        term_data = df
    else:
        raise ValueError("change_type must be ADDED or REMOVED")
    topic_data = terms_changed_lda(lda, vectorizer, term_data,
                                   change_type=change_type,
                                   text_processor=text_processor)
    return term_data.join(pd.DataFrame(topic_data))


def top_terms_plot_lda(text_processor, terms_df, change_type):
    if change_type == ADDED:
        tokens = get_top_term_tokens(
            text_processor, terms_df, change_type)
        fmt_arg = "Added"
    elif change_type == REMOVED:
        tokens = get_top_term_tokens(text_processor, terms_df, change_type)
        fmt_arg = "Removed"
    term_vecs = np.array([t.vector for t in tokens])
    pca = PCA(n_components=2).fit(term_vecs)
    dcmp_vals = pca.fit_transform(term_vecs)
    xy = dcmp_vals.transpose()
    plt.scatter(xy[0], xy[1], c=[COLORS[i] for i in terms_df.topic_1])
    texts = [
        plt.annotate(tokens[i].text, dcmp_vals[i])
        for i in range(len(dcmp_vals))
    ]
    plt.xlabel("PCA1")
    plt.ylabel("PCA2")
    plt.title(f"LDA Top Topic for 60 Most Frequently {fmt_arg} Terms")
    plt.show()
    return dcmp_vals


def mean_score_change_per_topic(terms_df):
    topics = terms_df.topic_1.unique()
    rows = []
    for t in topics:
        data = terms_df[terms_df.topic_1 == t]
        rows.append({
            "mean_score_change": data.mean_score_change.mean(),
            "mean_score": data.mean_compliance_score.mean(),
            "count": len(data),
            "topic": t
        })
    return pd.DataFrame(
        sorted(
            rows,
            key=lambda x: x["count"],
            reverse=True
        )
    )


def mean_score_change_per_topic_plot(terms_df):
    data = mean_score_change_per_topic(terms_df)
    fig, ax = plt.subplots()
    labels = [f"T{i}" for i in data.topic]
    colors = [COLORS[i] for i in data.topic]
    mean_score_changes = [i for i in data.mean_score_change]
    counts = [i for i in data["count"]]
    x = np.arange(len(labels))
    ax.bar(x - 0.4, counts)
    ax.bar(x + 0.4, mean_score_changes)
    fig.tight_layout()
    plt.show()


def visualize_document_topics(lda: LatentDirichletAllocation,
                              vectorizer: CountVectorizer,
                              text: spacy.tokens.doc.Doc,
                              granularity: str = SENTENCE,
                              text_processor: TextProcessor = None):
    sentences = document_topics(
        lda, vectorizer, text, granularity, text_processor)
    sentence_topics = [(s[0].text, list(s[1].keys())[0]) for s in sentences]
    viz = [
        f'<span style="background-color: {COLORS[s[1]]};">{s[0]}</span>'
        for s in sentence_topics
    ]
    return "<p>".join([s for s in viz])


def topics_bar_graph(results, category_names):
    """
    Parameters
    ----------
    results : dict
        A mapping from question labels to a list of answers per category.
        It is assumed all lists contain the same number of entries and that
        it matches the length of *category_names*.
    category_names : list of str
        The category labels.
    """
    labels = list(results.keys())
    data = np.array(list(results.values()))
    data_cum = data.cumsum(axis=1)
    category_colors = plt.get_cmap('RdYlGn')(
        np.linspace(0.15, 0.85, data.shape[1]))

    fig, ax = plt.subplots(figsize=(9.2, 5))
    ax.invert_yaxis()
    ax.xaxis.set_visible(False)
    ax.set_xlim(0, np.sum(data, axis=1).max())

    for i, (colname, color) in enumerate(zip(category_names, category_colors)):
        widths = data[:, i]
        starts = data_cum[:, i] - widths
        ax.barh(labels, widths, left=starts, height=0.5,
                label=colname, color=color)
        xcenters = starts + widths / 2

        r, g, b, _ = color
        text_color = 'white' if r * g * b < 0.5 else 'darkgrey'
        for y, (x, c) in enumerate(zip(xcenters, widths)):
            ax.text(x, y, str(int(c)), ha='center', va='center',
                    color=text_color)
    ax.legend(ncol=len(category_names), bbox_to_anchor=(0, 1),
              loc='lower left', fontsize='small')

    return fig, ax


def show_topic_dist(transformed):
    result = []
    for d in transformed:
        for entry in d:
            first, second, third, *rest = entry[1].items()
            result.append({
                "topic_rank_1": first[0],
                "topic_rank_1_score": first[1],
                "topic_rank_2": second[0],
                "topic_rank_2_score": second[1],
                "topic_rank_3": third[0],
                "topic_rank_3_score": third[1],
                "entry_text": entry[0]
            })
    return result


def concat_topic_data(input_df, topic_data):
    df = input_df
    df['topic_rank_1'] = topic_data.topic_rank_1
    df['topic_rank_1_score'] = topic_data.topic_rank_1_score
    df['topic_rank_2'] = topic_data.topic_rank_2
    df['topic_rank_2_score'] = topic_data.topic_rank_2_score
    df['topic_rank_3'] = topic_data.topic_rank_3
    df['topic_rank_3_score'] = topic_data.topic_rank_3_score
    return df


EXAMPLES = [
    "pilot",
    "cloud",
    "cloud",
    "computing",
    "means",
    "deliver",
    "fully",	
    "adopt",	
    "meantime",
    "October",
    "reviewer",
    "reviewer",
    "better", 	
    "average",
    "total",
    "traffic",
    "code",	
    "especially",	
    "equipment",
    "robotic",
    "theoretical",
    "capacity",
    "specialized",
    "space"
]
