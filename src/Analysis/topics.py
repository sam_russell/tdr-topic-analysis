import asyncio
from datetime import datetime
from decimal import Decimal
from functools import reduce, partial
import math
import os
import time
from typing import List, Callable

import pandas as pd

from gensim.models import LdaModel, TfidfModel
from gensim.corpora import Dictionary
from gensim.models.phrases import Phrases, Phraser

import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lookups import Lookups

from src.Models.predict_model import PredictModel, NLP, prune_tokens


def default_predict_model():
    pm = PredictModel.from_json(json_fpath="test_builder.json")
    pm.build_model()
    return pm



def word_count(word, doc):
    vector_norms = [t.vector_norm for t in doc]
    return vector_norms.count(word.vector_norm)


def contains_word(word, doc):
    for token in doc:
        if token.vector_norm == word.vector_norm:
            print(f"found {word.norm_}")
            return True
    return False



class TFIDF_:

    def __init__(self, docs):
        dataset = [[token.norm_ for token in doc] for doc in docs]
        self.dataset = dataset
        self.dictionary = Dictionary(dataset)
        self.corpus = None
        self.model = None

    def make_corpus(self):
        return [self.dictionary.doc2bow(doc) for doc in self.dataset]

    def get_tfidf_model(self):
        return TfidfModel(self.corpus)

    @classmethod
    def make(cls, docs):
        inst = cls(docs)
        inst.corpus = inst.make_corpus()
        inst.model = inst.get_tfidf_model()
        return inst

    def apply_model(self, doc_idx=None, doc=None):
        if doc_idx:
            return self.model[self.corpus[doc_idx]]
        elif doc:
            return self.model[doc]

    def apply_model_str(self, doc_idx=None, doc=None):
        app = self.apply_model(doc_idx=doc_idx, doc=doc)
        weights = {self.dictionary[pair[0]]: pair[1] for pair in app}
        return sorted(weights.items(), key=lambda x: x[1], reverse=True)

    def make_all(self, limit=None, filter_by=None):
        weights = [self.apply_model_str(doc=doc) for doc in self.corpus]
        if limit:
            return [i[:limit]for i in weights]
        return weights


class TermSet:
    ADDED = 1
    REMOVED = 2




def get_weighted_terms(tfidf=None,
                       predict=None):
    removed = {
        "repository_name": [],
        "repository_id": [],
        "interval": [],
        "score_change": [],
        "score_a": [],
        "score_b": [],
        "term_removed": [],
        "term_weight": [],
        "token_vector_norm": [],
        "token_lex_id": [],
        "sentence": [],
        "sentence_vector_norm": [],
        "similarity": [],
        "mean_similarity_a": [],
        "mean_similarity_b": [],
        "vector_norm_a": [],
        "vector_norm_b": [],
        "terms_difference_count": []
    }
    added = {
        "repository_name": [],
        "repository_id": [],
        "interval": [],
        "score_change": [],
        "score_a": [],
        "score_b": [],
        "term_added": [],
        "term_weight": [],
        "token_vector_norm": [],
        "token_lex_id": [],
        "sentence": [],
        "sentence_vector_norm": [],
        "similarity": [],
        "mean_similarity_a": [],
        "mean_similarity_b": [],
        "vector_norm_a": [],
        "vector_norm_b": [],
        "terms_difference_count": []
    }
    for interval in predict._model:
        for comp in interval:
            doc_a = tfidf.apply_model_str(
                doc=tfidf.dictionary.doc2bow([i.norm_ for i in comp._doc_a])
            )
            doc_a = dict(doc_a)
            keys_a = list(doc_a.keys())
            rmv = sorted(
                {
                    k: doc_a[k]
                    for k in
                    [i for i in comp.terms_removed if i in keys_a]
                }.items(),
                key=lambda x: x[1],
                reverse=True
            )
            rmv = dict(rmv)
            for k, v in rmv.items():
                score_a = predict.cumulative_score(comp._assmt_a)
                score_b = predict.cumulative_score(comp._assmt_b)
                score_change = score_b - score_a
                
                removed["repository_name"].append(comp.repository_name)
                removed["repository_id"].append(comp.repository_id)
                removed["score_a"].append(score_a)
                removed["score_b"].append(score_b)
                removed["score_change"].append(score_change)
                removed["interval"].append(comp.comparison_name)
                removed["term_weight"].append(v)
                removed["term_removed"].append(k)
                removed["token_vector_norm"].append([
                    t.vector_norm for t in comp.removed_tokens[k]
                ])
                removed["token_lex_id"].append([
                    t.lex_id for t in comp.removed_tokens[k]
                ])
                removed["sentence"].append([
                    t.span.text for t in comp.removed_tokens[k]
                ])
                removed["sentence_vector_norm"].append([
                    t.span.vector_norm for t in comp.removed_tokens[k]
                ])
                removed["similarity"].append(comp.similarity)
                removed["mean_similarity_a"].append(
                    predict.compare_cross(comp._doc_a).mean()
                    )
                removed["mean_similarity_b"].append(
                    predict.compare_cross(comp._doc_b).mean()
                    )
                removed["vector_norm_a"].append(comp.vector_norm_a)
                removed["vector_norm_b"].append(comp.vector_norm_b)
                removed["terms_difference_count"].append(
                    len(comp.terms_difference)
                )

            doc_b = tfidf.apply_model_str(
                doc=tfidf.dictionary.doc2bow([i.norm_ for i in comp._doc_b])
            )
            doc_b = dict(doc_b)
            keys_b = list(doc_b.keys())
            add = sorted(
                {
                    k: doc_b[k]
                    for k in
                    [i for i in comp.terms_added if i in keys_b]
                }.items(),
                key=lambda x: x[1],
                reverse=True
            )
            add = dict(add)
            for k, v in add.items():
                score_a = predict.cumulative_score(comp._assmt_a)
                score_b = predict.cumulative_score(comp._assmt_b)
                score_change = score_b - score_a
                
                added["repository_name"].append(comp.repository_name)
                added["repository_id"].append(comp.repository_id)
                added["score_change"].append(score_change)
                added["score_a"].append(score_a)
                added["score_b"].append(score_b)
                added["interval"].append(comp.comparison_name)
                added["term_weight"].append(v)
                added["term_added"].append(k)
                added["token_vector_norm"].append([
                    t.vector_norm for t in comp.added_tokens[k]
                ])
                added["token_lex_id"].append([
                    t.lex_id for t in comp.added_tokens[k]
                ])
                added["sentence"].append([
                    t.span.text for t in comp.added_tokens[k]
                ])
                added["sentence_vector_norm"].append([
                    t.span.vector_norm for t in comp.added_tokens[k]
                ])
                added["similarity"].append(comp.similarity)
                added["mean_similarity_a"].append(
                    predict.compare_cross(comp._doc_a).mean()
                    )
                added["mean_similarity_b"].append(
                    predict.compare_cross(comp._doc_b).mean()
                    )
                added["vector_norm_a"].append(comp.vector_norm_a)
                added["vector_norm_b"].append(comp.vector_norm_b)
                added["terms_difference_count"].append(
                    len(comp.terms_difference)
                    )
    return {"added": added, "removed": removed}


def make_dataframe(weighted_terms):
    pass


def quantile_tfidf_weights(df):
    qtl = list(df.term_weight.quantile([0.25, 0.75]))
    return {
        "high": pd.DataFrame([
            row for idx, row in df.iterrows()
            if row.term_weight > qtl[1]
            ]),
        "mid": pd.DataFrame([
            row for idx, row in df.iterrows()
            if row.term_weight > qtl[0]
            and row.term_weight < qtl[1]
            ]),
        "low": pd.DataFrame([
            row for idx, row in df.iterrows()
            if row.term_weight < qtl[0]
        ])
    }


def sort_term_freq(df):
    result = {}
    intervals = list(df.interval.unique())
    if "term_removed" in df.columns:
        term_label = "term_removed"
    else:
        term_label = "term_added"
    wt_sorted = quantile_tfidf_weights(df)
    for k, v in wt_sorted.items():
        for i in intervals:
            v = v[v.interval == i]
            counts = v.value_counts(term_label)
            freq = dict(zip([i for i in counts.index], [i for i in counts]))
            # freq_sorted is broken because BAD PANDAS!!
            freq_sorted = v.sort_values(by=term_label,
                                        key=lambda x: freq[str(x)],
                                        ascending=False)
            result[i][k] = freq_sorted
    return result


def top_n_terms(df, n=20):
    intervals = list(df.interval.unique())
    result = {k: {} for k in intervals}
    if "term_removed" in df.columns:
        term_label = "term_removed"
    else:
        term_label = "term_added"
    wt_sorted = quantile_tfidf_weights(df)
    for i in intervals:
        for k, v in wt_sorted.items():
            v = v[v.interval == i]
            counts = v.value_counts(term_label)
            freq = list(zip([i for i in counts.index], [i for i in counts]))
            result[i][k] = dict(freq[:n])
    return result


def make_tables(added, removed):
    fname = os.path.join("/", "home", "sam", "Desktop", "tdr_data")
    t = datetime.now().strftime('%FT%T')
    added = top_n_terms(added)
    removed = top_n_terms(removed)
    for interval, data in added.items():
        for k, v in data.items():
            df = pd.DataFrame([v])
            df = df.transpose()
            fname_pt1 = interval.replace("/", "_")
            df.to_csv(os.path.join(fname,
                                   f"terms_added_{fname_pt1}_{k}_{t}.csv"))
    for interval, data in removed.items():
        for k, v in data.items():
            df = pd.DataFrame([v])
            df = df.transpose()
            fname_pt1 = interval.replace("/", "_")
            df.to_csv(os.path.join(fname,
                                   f"terms_removed_{fname_pt1}_{k}_{t}.csv"))


def get_diffs(nlp, predict_model):
    result = {"added": [], "removed": []}
    for interval in predict_model._model:
        for rpt in interval:
            for k, v in prune_tokens(nlp, rpt.added_tokens).items():
                sentences = [t.sent.text for t in v]
                data = {
                    "repository_name": rpt.repository_name,
                    "repository_id": rpt.repository_id,
                    "term": k,
                    "usage": "  |  ".join(sentences),
                    "interval": rpt.comparison_name
                }
                result["added"].append(data)
            for k, v in prune_tokens(nlp, rpt.removed_tokens).items():
                sentences = [t.sent.text for t in v]
                data = {
                    "repository_name": rpt.repository_name,
                    "repository_id": rpt.repository_id,
                    "term": k,
                    "usage": "  |  ".join(sentences),
                    "interval": rpt.comparison_name
                }
                result["removed"].append(data)
    return result


def top_n_terms_usage(df_added, df_rem):
    added = [
        {
            "low": {
                i: " | ".join(df_added[df_added.term == i].usage)
                for i in ["technology", "develop", "public"]
            },
            "mid": {
                i: " | ".join(df_added[df_added.term == i].usage)
                for i in ["overview", "education", "suitable"]
            },
            "high": {
                i: " | ".join(df_added[df_added.term == i].usage)
                for i in ["gwdg", "cql", "wissenschaftliche"]
            }
        },
        {
            "low": {
                i: " | ".join(df_added[df_added.term == i].usage)
                for i in ["perform", "refer", "website"]
            },
            "mid": {
                i: " | ".join(df_added[df_added.term == i].usage)
                for i in ["expertise", "consult", "feedback"]
            },
            "high": {
                i: " | ".join(df_added[df_added.term == i].usage)
                for i in ["response", "meeting", "advisory"]
            }
        }
    ]

    removed = [
        {
            "low": {
                i: " | ".join(df_rem[df_rem.term == i].usage)
                for i in ["etc", "facility", "end"]
            },
            "mid": {
                i: " | ".join(df_rem[df_rem.term == i].usage)
                for i in ["write", "jhove", "guidelines"]
            },
            "high": {
                i: " | ".join(df_rem[df_rem.term == i].usage)
                for i in ["decide", "judgement", "thing"]
            }
        },
        {
            "low": {
                i: " | ".join(df_rem[df_rem.term == i].usage)
                for i in ["specification", "like", "entity"]
            },
            "mid": {
                i: " | ".join(df_rem[df_rem.term == i].usage)
                for i in ["obsolescence", "deep", "clearly"]
            },
            "high": {
                i: " | ".join(df_rem[df_rem.term == i].usage)
                for i in ["meta", "manageable", "google"]
            }
        }
    ]
    return {"added": added, "removed": removed}


def format_json_terms(x):
    output = []
    for j in x:
        data = {}
        for wt, terms in j.items():
            if not data.get(wt):
                data[wt] = {}
            for t, sents in terms.items():
                s = sents.replace(
                    " | ", "\n-------------------------------------\n")
                data[wt][t] = s
        output.append(data)
    if len(output) == 2:
        return {
            "2010_2014-2017": output[0],
            "2014-2017_2017-2019": output[1]
        }
    else:
        return dict(enumerate(output))


    
            
#  a = tfidf.apply_model_str(doc=tfidf.dictionary.doc2bow([i.norm_ for i in pm._model[0][0]._doc_a]))

# rmv = sorted({k: a_d[k] for k in [i for i in pm._model[0][0].terms_removed if i in keys]}, key=lambda x: x[1], reverse=True)

# [(i.text, i.lex_id, [t.lex_id for t in list(docs)[0]].count(i.lex_id)) for i in list(docs)[0]]

