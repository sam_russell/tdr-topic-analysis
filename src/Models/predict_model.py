from functools import reduce, partial
import json
import logging
import math
from typing import Dict, List, Callable

from bs4 import BeautifulSoup
import click
import gensim.models
import gensim.utils
from gensim.models import LdaModel, TfidfModel
from gensim.corpora import Dictionary
from gensim.models.phrases import Phrases, Phraser


import pandas as pd
import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lookups import Lookups

from src.Data.database_migration import SQLiteManager
from src.Data.text_models import AssessmentEntry, Assessment, Repository, \
    Guideline, GuidelinePackage
from src import utils

# DB = "data/interim/data_repository_assmts_2020-10-16T09:26:05.db"
DB = "data/interim/data_repository_assmts_2020-10-23T12:40:32.db"
REPO_QUERY = "src/Models/repositories_query.sql"

# NLP = spacy.load("en_vectors_web_lg")
NLP = spacy.load("en_core_web_lg")

logger = logging.getLogger(__name__)


def prune_tokens_2(nlp, terms_dict):
    print(terms_dict)
    pruned = {
        k: v for k, v in terms_dict.items()
        if not nlp.vocab[k].is_stop
        and not nlp.vocab[k].is_space
        and not nlp.vocab[k].is_punct
        and not nlp.vocab[k].is_digit
        and not nlp.vocab[k].like_url
        and not nlp.vocab[k].like_num
        # and nlp.vocab[k].is_oov
    }
    return pruned


def prune_tokens(nlp, doc):
    return [
        t for t in doc
        if not nlp.vocab[t].is_stop
        and not nlp.vocab[t].is_space
        and not nlp.vocab[t].is_punct
        and not nlp.vocab[t].is_digit
        and not nlp.vocab[t].like_url
        and not nlp.vocab[t].like_num
        # and nlp.vocab[t].is_oov
    ]


def prune_tokens_(nlp, doc):
    """SpaCy NLP pipeline component for removing extraneous text"""
    #    tokens = [token.lemma_ for token in doc]
    pruned = [
        t for t in doc
        if not nlp.vocab[t].is_stop
        and not nlp.vocab[t].is_space
        and not nlp.vocab[t].is_punct
        and not nlp.vocab[t].is_digit
        and not nlp.vocab[t].like_url
        and not nlp.vocab[t].like_num
        # and nlp.vocab[t].is_oov
    ]
    clean_doc = spacy.tokens.Doc(nlp.vocab, words=pruned)
    return clean_doc


def find_bigrams(nlp, predict_model):
    docs = predict_model.all_docs()
    s_docs = [
        [
            t.norm_ for t in doc
            if not t.is_stop
            and not t.is_space
            and not t.is_punct
            and not t.is_digit
            and not t.like_url
            and not t.like_num
        ]
        for doc in docs
    ]
    phrases = Phrases(s_docs, min_count=1, threshold=20.0)
    bigram = Phraser(phrases)
    return [bigram[doc] for doc in s_docs]


def get_bigram_phraser(predict_model):
    docs = predict_model.all_docs()
    s_docs = [
        [
            t.norm_ for t in doc
            if not t.is_stop
            and not t.is_space
            and not t.is_punct
            and not t.is_digit
            and not t.like_url
            and not t.like_num
        ]
        for doc in docs
    ]
    phrases = Phrases(s_docs, min_count=1, threshold=20.0)
    bigram = Phraser(phrases)
    return bigram


def find_trigrams(nlp, predict_model):
    docs = predict_model.all_docs()
    s_docs = [
        [
            t.norm_ for t in doc
            if not t.is_stop
            and not t.is_space
            and not t.is_punct
            and not t.is_digit
            and not t.like_url
            and not t.like_num
        ]
        for doc in docs
    ]
    phrases = Phrases(s_docs, min_count=1)
    bigram = Phraser(phrases)
    trigram = Phrases([bigram[doc] for doc in s_docs], min_count=1)
    return [trigram[doc] for doc in s_docs]


class AssessmentDocument:
    """Contains a spacy.tokens.doc.Doc instance, with contextual metadata"""

    def __init__(self,
                 nlp_inst: spacy.lang.en.English,
                 assmt_entry: AssessmentEntry,
                 **pipes):
        self.nlp = nlp_inst
        self.assmt_entry = assmt_entry
        self.doc = None
        self.pruned_doc = None
        self.bigram_doc = None
        for pipe_name, pipe_callable in pipes.items():
            if not self.nlp.has_pipe(pipe_name):
                component = partial(pipe_callable, nlp_inst)
                self.nlp.add_pipe(component, name=pipe_name)

    def preprocess_text(self) -> List[str]:
        """Takes models.Entry obj or string, lemmatizes and tokenizes"""
        tokens = [token.lemma_ for token in self.doc]
        return [
            t.lower() for t in tokens
            if not self.nlp.vocab[t].is_stop
            and not self.nlp.vocab[t].is_space
            and not self.nlp.vocab[t].is_punct
            and not self.nlp.vocab[t].is_digit
        ]

    @classmethod
    def make(cls,
             nlp_inst: spacy.lang.en.English,
             assmt_entry: AssessmentEntry):
        inst = cls(nlp_inst, assmt_entry)
        # inst = cls(nlp_inst, assmt_entry, prune_tokens=prune_tokens)
        try:
            inst.doc = inst.nlp(assmt_entry)
        except (AttributeError, TypeError):
            inst.doc = inst.nlp(assmt_entry.response)
        return inst


class TextCleaner:
    """Removes markup, stopwords, and bric-a-brac from texts"""

    def __init__(self, db_filepath):
        self.session = SQLiteManager.make(db_filepath).session
        self.docs = []

    def entries(self):
        entries = self.session.query(AssessmentEntry).all()
        return [i for i in entries if i.response]

    def entries_per_assessment(self):
        entries = self.entries()
        assmt_ids = [entry.assessment_id for entry in entries]
        return {
            i: "\n".join(
                [e.response for e in entries if e.assessment_id == i]
                )
            for i in assmt_ids
        }

    def preprocess_assessments(self):
        self.docs = {
            key: AssessmentDocument.make(
                NLP,
                BeautifulSoup(entry).get_text()
            )
            for key, entry in self.entries_per_assessment().items()
        }
        return self.docs
        # return [doc.preprocess_text() for doc in self.docs.values()]


class Term:
    """Represents a term in the corpus"""

    def __init__(
            self,
            repository_name=None,
            repository_id=None,
            interval=None,           
            score_change=None,
            score_a=None,
            score_b=None,
            term_weight=None,          
            token_vector_norm=None,
            token_lex_id=None,
            sentence=None,   
            sentence_vector_norm=None,
            similarity=None,      
            mean_similarity_a=None,
            mean_similarity_b=None, 
            vector_norm_a=None, 
            vector_norm_b=None, 
            terms_difference_count=None,
            term_removed=None,
            term_added=None,
            **kwargs
    ):
        self.repository_name        = repository_name
        self.repository_id          = repository_id
        self.interval               = interval
        self.score_change           = score_change
        self.score_a                = score_a
        self.score_b                = score_b
        self.term_changed           = term_added or term_removed
        self.term_weight            = term_weight
        self.token_vector_norm      = token_vector_norm
        self.token_lex_id           = token_lex_id
        self.sentence               = sentence
        self.sentence_vector_norm   = sentence_vector_norm
        self.similarity             = similarity
        self.mean_similarity_a      = mean_similarity_a
        self.mean_similarity_b      = mean_similarity_b
        self.vector_norm_a          = vector_norm_a
        self.vector_norm_b          = vector_norm_b
        self.terms_difference_count = terms_difference_count


class TFIDF:
    def __init__(self, docs):
        #docs = list(predict_model.all_docs())
        #docs = [t.norm_ for t in [prune_tokens(doc) for doc in docs]]
        #
        #    docs = [phraser[doc] for doc in docs]
        dictionary = Dictionary(docs)
        corpus = [dictionary.doc2bow(doc) for doc in docs]
        self.docs = docs
        self.dictionary = dictionary
        self.corpus = corpus
        self.tfidf = TfidfModel(corpus)
        self.lda = None

    def weighted_terms(self, doc_idx):
        weights = self.tfidf[self.corpus[doc_idx]]
        result = {
            self.dictionary[term_wt[0]]: term_wt[1]
            for term_wt in weights
        }
        return dict(sorted(result.items(), key=lambda x: x[1], reverse=True))

    def terms_by_quantile(
            self,
            quantiles: List[float] = [0, 0.25, 0.75, 1],
            selection: Callable = lambda q, x: q[0] <= x <= q[2]
    ):
        weights = [self.tfidf[doc] for doc in self.corpus]
        wtd_terms = [
            pd.DataFrame(wtd, columns=["term_id", "term_wt"])
            for wtd in weights
        ]
        result = []
        for wtd in wtd_terms:
            qtls = wtd.term_wt.quantile(quantiles)
            func = partial(selection, list(qtls))
            df = pd.DataFrame([
                row for idx, row in wtd.iterrows()
                # lambda qtls, val: qtls[low] <= val <= qtls[hi]
                if func(row.term_wt)
            ])
            str_terms = [self.dictionary[int(i)] for i in df.term_id]
            df["term_str"] = str_terms
            result.append(df)
        return result

    def filter_corpus_quantile(self, qtl_terms=None):
        if not qtl_terms:
            qtl_terms = self.terms_by_quantile()
        new_corpus = []
        for df in qtl_terms:
            new_doc = []
            for idx, row in df.iterrows():
                new_doc.append(
                    (int(idx), int(row.term_id))
                )
            new_corpus.append(new_doc)
        return new_corpus

    def LDA(self, num_topics, corpus=None):
        if not corpus:
            corpus = self.corpus
        # id2word = self.dictionary.id2token
        model = LdaModel(
            corpus=corpus,
            id2word=self.dictionary,
            chunksize=400,
            passes=200,
            iterations=1000,
            alpha='auto',
            eta='auto',
            num_topics=num_topics,
            eval_every=None,
            per_word_topics=True
            )
        self.lda = model
        return model


class Comparison:
    """Detailed representation of differences between documents"""

    def __init__(self,
                 doc_a=None,
                 doc_b=None,
                 assmt_a=None,
                 assmt_b=None,
                 similarity=None,
                 terms_difference=None,
                 terms_added=None,
                 terms_removed=None,
                 vector_a=None,
                 vector_norm_a=None,
                 vector_b=None,
                 vector_norm_b=None,
                 repository_name=None,
                 repository_id=None,
                 guideline_package_name_a=None,
                 guideline_package_name_b=None,
                 guideline_package_id_a=None,
                 guideline_package_id_b=None,
                 added_tokens=None,
                 removed_tokens=None,
                 *args, **kwargs):
        """Not meant to be called directly, use `make` factory method """
        self._doc_a = doc_a
        self._doc_b = doc_b
        self._assmt_a = assmt_a
        self._assmt_b = assmt_b
        self.similarity = similarity
        self.terms_difference = terms_difference
        self.terms_added = terms_added
        self.terms_removed = terms_removed
        self.added_tokens = added_tokens
        self.removed_tokens = removed_tokens
        self.vector_a = vector_a
        self.vector_norm_a = vector_norm_a
        self.vector_b = vector_b
        self.vector_norm_b = vector_norm_b
        self.repository_name = repository_name
        self.repository_id = repository_id
        self.guideline_package_name_a = guideline_package_name_a
        self.guideline_package_name_b = guideline_package_name_b
        self.guideline_package_id_a = guideline_package_id_a
        self.guideline_package_id_b = guideline_package_id_b
        self.comparison_name = "/".join([guideline_package_name_a,
                                         guideline_package_name_b])

    @staticmethod
    def L2_norm(vector):
        """Computes vector magnitude"""
        return math.sqrt(reduce(lambda x, y: x + y, [i * i for i in vector]))

    @classmethod
    def compare_docs(cls, texts, a, b):
        similarity = texts[a.id].doc.similarity(texts[b.id].doc)
        # for each instance of i.norm_, there must be a dictionary
        # mapping to a list of Term instances
        a_terms = set([i.norm_ for i in texts[a.id].doc])
        b_terms = set([i.norm_ for i in texts[b.id].doc])
        vector_a = texts[a.id].doc.vector
        vector_b = texts[b.id].doc.vector
        vector_norm_a = cls.L2_norm(vector_a)
        vector_norm_b = cls.L2_norm(vector_b)
        difference = a_terms.symmetric_difference(b_terms)
        terms_added = [i for i in difference if i not in a_terms]
        added_tokens = {
            norm: [t for t in texts[b.id].doc if t.norm_ == norm]
            for norm in terms_added
        }
        terms_removed = [i for i in difference if i not in b_terms]
        removed_tokens = {
            norm: [t for t in texts[a.id].doc if t.norm_ == norm]
            for norm in terms_removed
        }
        return {
            "similarity": similarity,
            "terms_difference": difference,
            "terms_added": terms_added,
            "terms_removed": terms_removed,
            "added_tokens": added_tokens,
            "removed_tokens": removed_tokens,
            "vector_a": vector_a,
            "vector_b": vector_b,
            "vector_norm_a": vector_norm_a,
            "vector_norm_b": vector_norm_b
        }

    @classmethod
    def make(cls, session, texts, repo_id, pkg_id_a, pkg_id_b):
        """Comparison factory method"""
        assmts = session.query(
            Assessment).filter_by(repository_id=repo_id).all()

        (a, b, *rest) = sorted(
            set(
                [i for i in assmts if i.guideline_package_id == pkg_id_a]
            ).union(
                set([i for i in assmts if i.guideline_package_id == pkg_id_b])
            ),
            key=lambda x: x.guideline_package.name
        )
        # there is only one (two?) case, ICPSR 2010 DSA assessment, where
        # multiple assessments exist for a single repository, we
        # choose to ignore the earlier, "pilot" version, at this time.
        if not b:
            raise ValueError("No assessment found for pkg_id_b")
        if rest:
            print("there was more than one")
            a = b
            b = rest[0]
        # reassigning a and b to date sequential context
        first, second = sorted(
            [a, b],
            key=lambda x: x.guideline_package.name
        )
        print(f"first: {first.guideline_package.name}, "
              f"second: {second.guideline_package.name}")
        a, b = first, second
        sims = cls.compare_docs(texts, a, b)
        inst = cls(
            assmt_a=a,
            assmt_b=b,
            doc_a=texts[a.id].doc,
            doc_b=texts[b.id].doc,
            repository_name=a.repository.name,
            repository_id=repo_id,
            guideline_package_name_a=a.guideline_package.name,
            guideline_package_name_b=b.guideline_package.name,
            guideline_package_id_a=a.guideline_package_id,
            guideline_package_id_b=b.guideline_package_id,
            **sims
        )
        return inst

    @classmethod
    def make_set(cls, session, texts, pkg_id_a, pkg_id_b):
        """Initializes Comparisons for a set of asssessments"""
        comparisons = []
        for repo_id in set(
                [
                    i.repository_id for i in session.query(Assessment).all()
                ]
        ):
            try:
                c = cls.make(session, texts, repo_id, pkg_id_a, pkg_id_b)
                comparisons.append(c)
            except ValueError as e:
                print(e)
                comparisons.append(0)
        return comparisons

    @classmethod
    def make_all(cls, session, texts):
        """Initializes Comparisons for all assessments"""
        dsa2010_dsa2014 = cls.make_set(session, texts, 1, 2)
        dsa2014_cts2017 = cls.make_set(session, texts, 2, 3)
        dsa2010_cts2017 = cls.make_set(session, texts, 1, 3)
        output = [dsa2010_dsa2014, dsa2014_cts2017, dsa2010_cts2017]
        return [list(filter(lambda x: x, i)) for i in output]


class PredictModel:

    def __init__(self,
                 db_filepath=None,
                 output_filepath=None):
        self.db_filepath = db_filepath
        self.output_filepath = output_filepath
        self.text_cleaner = None
        self.start_timestamp = None
        self._model = None
        self.tfidf = None
        self.lda = None

    @classmethod
    def from_json(cls, json_str=None, json_fpath=None):
        if json_str and json_fpath:
            raise TypeError
        if json_str:
            data = json.loads(json_str)
        else:
            with open(json_fpath, "r") as f:
                data = json.loads(f.read())
        return cls(db_filepath=data["sqlite_filepath"])

    def build_model(self):
        tc = TextCleaner(self.db_filepath)
        texts = tc.preprocess_assessments()
        doc_data = Comparison.make_all(tc.session, texts)
        # bigram_phraser = get_bigram_phraser(self)
        self.text_cleaner = tc
        self._model = doc_data
        self.tfidf = TFIDF(self.all_docs())
        return doc_data

    def all_docs(self):
        all_comps = reduce(
            lambda x, y: x + y,
            [grp for grp in self._model],
            []
        )
        all_docs = set([i._doc_a for i in all_comps]
                       + [i._doc_b for i in all_comps])
        return all_docs

    def compare_cross(self, doc):
        docs = [i for i in self.all_docs() if i != doc]
        sims_dist = [doc.similarity(i) for i in docs]
        return pd.Series(sims_dist)

    def similarity_cross_product(self):
        return [self.compare_cross(d) for d in self.all_docs()]

    def compare_cross_product_(self):
        data = self._model
        mean_sims = []
        for comp_group in data:
            all_comps = list(
                reduce(lambda x, y: x + y,
                       [grp for grp in data], [])
                )
            for comp in all_comps:
                all_docs = [i._doc_a for i in all_comps] \
                    + [i._doc_b for i in all_comps]
                all_docs = set(all_docs)
                for doc in all_docs:
                    comp_docs = [i for i in all_docs if i != doc]
                    sims = [doc.similarity(i) for i in comp_docs]
                    mean_sim = sum(sims) / len(sims)
                    mean_sims.append(mean_sim)
        return mean_sims

    def cumulative_score(self, assmt):
        session = self.text_cleaner.session
        entries = session.query(
            AssessmentEntry
        ).filter_by(
            assessment_id=assmt.id
        ).all()
        return sum([
            i.compliance_level for i in entries
            if i.guideline_id not in [51, 34, 33]
        ])

    def sort_by_vector_norm(self, tokens_list):
        """Sorts a list of tokens by their L2 norm"""
        return sorted(
            tokens_list,
            key=lambda x: NLP.vocab[x].vector_norm,
            reverse=True
        )

    def to_table(self):
        data = reduce(lambda x, y: x + y, self._model, [])
        table = [{
            "comparison": i.comparison_name,
            "repository_id": i.repository_id,
            "repository_name": i.repository_name,
            "score_a": self.cumulative_score(i._assmt_a),
            "score_b": self.cumulative_score(i._assmt_b),
            "score_change": (self.cumulative_score(i._assmt_a) * -1)
            + self.cumulative_score(i._assmt_b),
            "similarity": i.similarity,
            "mean_similarity_a": self.compare_cross(i._doc_a).mean(),
            "mean_similarity_b": self.compare_cross(i._doc_b).mean(),
            "vector_norm_a": i.vector_norm_a,
            "vector_norm_b": i.vector_norm_b,
            "terms_difference_count": len(i.terms_difference),
            "terms_difference": utils.comma_sep_string(
                self.sort_by_vector_norm(i.terms_difference)
            ),
            "terms_added_count": len(i.terms_added),
            "terms_added": utils.comma_sep_string(
                self.sort_by_vector_norm(i.terms_added)
            ),
            "terms_removed_count": len(i.terms_removed),
            "terms_removed": utils.comma_sep_string(
                self.sort_by_vector_norm(i.terms_removed)
            )
        } for i in data]
        return table

    def to_df(self, *pipes):
        table = self.to_table()
        if pipes:
            for p in pipes:
                table = p(table)
        df = pd.DataFrame(table)
        return df

    def to_csv(self, output_filepath):
        df = self.to_df()
        df.to_csv(output_filepath, index=False)
