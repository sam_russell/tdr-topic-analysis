from collections import namedtuple
from functools import reduce, partial
import json
import logging
import math
import random
import re
from typing import Dict, List, Callable

from bs4 import BeautifulSoup
import click
import gensim.models
import gensim.utils
from gensim.models import LdaModel, TfidfModel
from gensim.corpora import Dictionary
from gensim.models.phrases import Phrases, Phraser
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer


import pandas as pd
import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lookups import Lookups
from spacy import pipeline
from spacy.tokens import Token

from src.Data.database_migration import SQLiteManager
from src.Data.text_models import AssessmentEntry, Assessment, Repository, \
    Guideline, GuidelinePackage
from src import utils

DB = "data/interim/data_repository_assmts_2020-10-23T12:40:32.db"
REPO_QUERY = "src/Models/repositories_query.sql"

NLP = spacy.load("en_core_web_lg")

LINE_BREAK_PAT = re.compile("[\n\r|\n]")

logger = logging.getLogger(__name__)

Token.set_extension("is_break", default=False, force=True)


def filter_tokens(tokens):
    return [
        t for t in tokens
        if not t.is_stop
        and not t.is_space
        and not t.is_punct
        and not t.is_digit
        and not t.like_url
        and not t.like_num
        and t.is_alpha
        and "ENTRYBREAK" not in t.text
        and t.text != 'n'
    ]



def prune_tokens(nlp, doc):
    """SpaCy NLP pipeline component for removing extraneous text"""
    #    tokens = [token.lemma_ for token in doc]
    pruned = [
        t.text for t in doc
        if not nlp.vocab[t.text].is_stop
        and not  nlp.vocab[t.text].is_space
        and not  nlp.vocab[t.text].is_punct
        and not  nlp.vocab[t.text].is_digit
        and not  nlp.vocab[t.text].like_url
        and not  nlp.vocab[t.text].like_num
        # and nlp.vocab[t].is_oov
    ]
    clean_doc = spacy.tokens.Doc(nlp.vocab, words=pruned)
    return clean_doc


def concat_chunks(doc):
    return reduce(lambda x, y: [t for t in x] + [t for t in y], doc.noun_chunks)


def merge_noun_chunks_no_stops(doc):
    """Merge noun chunks into a single token.
    doc (Doc): The Doc object.
    RETURNS (Doc): The Doc object with merged noun chunks.
    DOCS: https://spacy.io/api/pipeline-functions#merge_noun_chunks
    """
    if not doc.is_parsed:
        return doc
    with doc.retokenize() as retokenizer:
        for np in doc.noun_chunks:
            attrs = {"tag": np.root.tag, "dep": np.root.dep}
            first_not_stop = [
                t for t in np if not t.is_stop
                and not t.is_punct
                and not t.is_digit
                and not t.like_url
            ]
            if not first_not_stop:
                new_span = np
            else:
                new_span = doc[first_not_stop[0].i : np.end]
            retokenizer.merge(
                new_span,
                attrs=attrs
            )
    return doc


def entry_break_recognizer(doc):
    """Pipeline component to add entry break information to doc"""
    with doc.retokenize():
        for t in doc:
            if "ENTRYBREAK" in t.text:
                t._.is_break = True
            else:
                t._.is_break = False
    return doc


class AssessmentDocument:
    """Contains a spacy.tokens.doc.Doc instance, with contextual metadata"""
    def __init__(
            self,
            nlp_inst: spacy.lang.en.English,
            assmt_entry: AssessmentEntry,
            assmt,
            *pipes,
            **user_def_pipes
    ):
        self.nlp = nlp_inst
        self.assmt_entry = assmt_entry
        self.assmt = assmt
        self.doc = None
        self.pruned_doc = None
        self.bigram_doc = None
        self.processor = None
        for pipe in pipes:
            if not self.nlp.has_pipe(pipe):
                p = self.nlp.create_pipe(pipe)
                self.nlp.add_pipe(p)
        for pipe_name, pipe_callable in user_def_pipes.items():
            if not self.nlp.has_pipe(pipe_name):
                # component = partial(pipe_callable, nlp_inst)
                component = pipe_callable
                self.nlp.add_pipe(component, name=pipe_name, after="ner")

    @classmethod
    def make(cls,
             nlp_inst: spacy.lang.en.English,
             assmt_entry: AssessmentEntry,
             assmt,
             **kwargs):
        inst = cls(
            nlp_inst,
            assmt_entry,
            assmt,
            entry_break_recognizer=entry_break_recognizer,
            #"merge_entities",
            **kwargs
            # merge_noun_chunks_no_stops=merge_noun_chunks_no_stops
            #prune_tokens=prune_tokens
        )
        # inst = cls(nlp_inst, assmt_entry, prune_tokens=prune_tokens)
        try:
            inst.doc = inst.nlp(assmt_entry)
        except (AttributeError, TypeError) as er:
            print(er)
            pass
        return inst

    def with_processor(self, processor):
        self.processor = processor
        return self


class TextCleaner:
    """Removes markup, stopwords, and bric-a-brac from texts"""

    def __init__(self, db_filepath):
        self.session = SQLiteManager.make(db_filepath).session
        self.docs = []

    def entries(self):
        entries = self.session.query(AssessmentEntry).all()
        return [i for i in entries if i.response]

    def entries_per_assessment(self):
        entries = self.entries()
        assmt_ids = [entry.assessment_id for entry in entries]
        return {
            i: (
                "/n/n/n/n/n     ENTRYBREAK     /n/n/n/n/n".join(
                    [e.response for e in entries if e.assessment_id == i]
                ),
                [e for e in entries if e.assessment_id == i][0].assessment
            )
            for i in assmt_ids
        }

    def preprocess_assessments(self, nlp, merge_noun_chunks=False):
        kwargs = {}
        if merge_noun_chunks:
            kwargs["merge_noun_chunks_no_stops"] = merge_noun_chunks_no_stops
        self.docs = {
            key: AssessmentDocument.make(
                nlp,
                assmt_entry=re.sub(
                    LINE_BREAK_PAT, " ", BeautifulSoup(entry[0]).get_text()
                ),
                assmt=entry[1],
                **kwargs
            )
            for key, entry in self.entries_per_assessment().items()
        }
        return self.docs
        # return [doc.preprocess_text() for doc in self.docs.values()]


def split_article_from_nc(doc, token):
    with doc.retokenize() as rtk:
        ws_sep = token.orth_.split(' ')
        to_split = [i for i in ws_sep if doc.vocab[i].is_stop]
        if not to_split:
            return None
        else:
            pass


class TFIDF:
    """Container for SciKit-Learn term frequency models"""
    def __init__(self):
        self.term_count_vectorizer = None
        self.term_count_feature_names = None
        self.term_count_docs = []
        self.term_count_data_csr = None
        # self.term_count_data_array = None
        self.tfidf_vectorizer = None
        self.tfidf_docs = []
        self.tfidf_data_csr = None
        # self.tfidf_data_array = None
        self.tfidf_feature_names = None

    def get_tfidf_weights(self, assmt_doc):
        result = {}
        array_idx = assmt_doc.assmt.id - 1
        sparse_td_matrix, = self.tfidf_data_csr[array_idx].toarray()
        for i in range(len(sparse_td_matrix)):
            tf = sparse_td_matrix[i]
            if tf:
                result[self.tfidf_feature_names[i]] = tf
        return result

    @classmethod
    def make(cls, text_processor, analyzer: Callable = None,
             max_df=0.95, min_df=2):
        """Factory classmethod for initializing all term frequency models"""
        # creating uninitialized struct
        inst = cls()
        
        # Randomly selecting 1 document per repository to avoid
        # biasing the LDA and TF-IDF models in favor of
        # repositories that have multiple recertifications.
        unique_repo = set([
            d.assmt.repository.id for d in
            text_processor.docs.values()
        ])
        repo = {i: [] for i in unique_repo}
        for d in text_processor.docs.values():
            repo[d.assmt.repository.id].append(d)
        for repo_docs in repo.values():
            inst.term_count_docs.append(random.choice(repo_docs))

        # Defining an analyzer function so we can use the more sophisticated
        # SpaCy text processing pipeline instead of the default SciKit-Learn
        # tokenizers and stop word filters.
        def analyzer_func(tokens):
            return [t.lemma_ for t in filter_tokens(tokens)]
        # Supplying a callable for optional parameter `filter_func` will
        # override this definition.
        if not analyzer:
            analyzer = analyzer_func

        # Initializing the SciKit-Learn CountVectorizer with documents
        # split into their 16 entries because it is less perplexing for
        # latent Dirichlet allocation.
        data = reduce(
            lambda x, y: y + x,
            [
                text_processor.get_doc_entries(d.doc)
                for d in inst.term_count_docs
            ], []
        )
        inst.term_count_vectorizer = CountVectorizer(data, analyzer=analyzer)
        inst.term_count_data_csr = inst.term_count_vectorizer.fit_transform(
            data)
        tc_term_lookup = inst.term_count_vectorizer.get_feature_names()
        inst.term_count_feature_names = tc_term_lookup
        # Term count model now fully initialized

        # Initializing SciKit-Learn TF-IDF model with the same analyzer,
        # but using the entire corpus and processing each document as a
        # whole to improve weight accuracy.
        data = [d.doc for d in text_processor.docs.values()]
        inst.tfidf_vectorizer = TfidfVectorizer(
            data,
            analyzer=analyzer,
            max_df=max_df,
            min_df=min_df
        )
        inst.tfidf_data_csr = inst.tfidf_vectorizer.fit_transform(data)
        tfidf_term_lookup = inst.tfidf_vectorizer.get_feature_names()
        inst.tfidf_feature_names = tfidf_term_lookup
        return inst


class TFIDF_:
    def __init__(self, docs):
        #docs = list(predict_model.all_docs())
        #docs = [t.norm_ for t in [prune_tokens(doc) for doc in docs]]
        #
        #    docs = [phraser[doc] for doc in docs]
        dictionary = Dictionary(docs)
        corpus = [dictionary.doc2bow(doc) for doc in docs]
        self.docs = docs
        self.dictionary = dictionary
        self.corpus = corpus
        self.tfidf = TfidfModel(corpus)
        self.lda = None

    def weighted_terms(self, doc_idx):
        weights = self.tfidf[self.corpus[doc_idx]]
        result = {
            self.dictionary[term_wt[0]]: term_wt[1]
            for term_wt in weights
        }
        return dict(sorted(result.items(), key=lambda x: x[1], reverse=True))

    def terms_by_quantile(
            self,
            quantiles: List[float] = [0, 0.1, 0.33, 1],
            selection: Callable = lambda q, x: q[0] <= x <= q[2]
    ):
        weights = [self.tfidf[doc] for doc in self.corpus]
        wtd_terms = [
            pd.DataFrame(wtd, columns=["term_id", "term_wt"])
            for wtd in weights
        ]
        result = []
        for wtd in wtd_terms:
            qtls = wtd.term_wt.quantile(quantiles)
            func = partial(selection, list(qtls))
            df = pd.DataFrame([
                row for idx, row in wtd.iterrows()
                # lambda qtls, val: qtls[low] <= val <= qtls[hi]
                if func(row.term_wt)
            ])
            str_terms = [self.dictionary[int(i)] for i in df.term_id]
            df["term_str"] = str_terms
            result.append(df)
        return result

    def filter_corpus_quantile(self, qtl_terms=None):
        if not qtl_terms:
            qtl_terms = self.terms_by_quantile()
        new_corpus = []
        for df in qtl_terms:
            new_doc = []
            for idx, row in df.iterrows():
                new_doc.append(
                    (int(idx), int(row.term_id))
                )
            new_corpus.append(new_doc)
        return new_corpus

    def LDA(self, num_topics, corpus=None):
        if not corpus:
            corpus = self.corpus
        # id2word = self.dictionary.id2token
        model = LdaModel(
            corpus=corpus,
            id2word=self.dictionary,
            chunksize=400,
            passes=20,
            iterations=60,
            alpha='auto',
            eta='auto',
            num_topics=num_topics,
            eval_every=None,
            per_word_topics=True,
            minimum_probability=0.01
            )
        self.lda = model
        return model


def get_tfidf(text_cleaner):
    docs = [
        [t.lemma_.lower() for t in filter_tokens(rpt_doc.doc)]
        for rpt_doc in text_cleaner.docs.values()
    ]
    return TFIDF(docs)


class TextProcessor:
    def __init__(self, text_cleaner, tfidf=None):
        self.text_cleaner = text_cleaner
        self.tfidf = tfidf
        self.docs = {}

    @classmethod
    def make(cls, text_cleaner, nlp=None, merge_noun_chunks=False):
        if not text_cleaner.docs:
            text_cleaner.preprocess_assessments(nlp, merge_noun_chunks)
        inst = cls(text_cleaner)
        try:
            inst.docs = {
                k: v.with_processor(inst)
                for k, v in text_cleaner.docs.items()
            }
        except Exception as e:
            print(e)
        inst.tfidf = TFIDF.make(inst)
        return inst

    @staticmethod
    def get_doc_entries(doc):
        breaks = [t for t in doc if t._.is_break]
        print(f"num breaks: {len(breaks)}")
        #breaks = [t for t in doc if "ENTRY_BREAK" in t.text]
        indices = [
            (breaks[t].i, breaks[t+1].i)
            for t in range(len(breaks) - 1)
        ]
        return [doc[idx[0] + 1: idx[1]].as_doc() for idx in indices]

    def get_all_doc_entries(self, randomized=False):
        if randomized:
            unique_repo = set([d.assmt.repository.id for d in self.docs.values()])
            repo = {i: [] for i in unique_repo}
            for d in self.docs.values():
                repo[d.assmt.repository.id].append(d)
            return reduce(
                lambda x, y: y + x,
                [self.get_doc_entries(random.choice(v).doc) for v in repo.values()],
                []
            )
        else:
            return reduce(
                lambda x, y: y + x,
                [self.get_doc_entries(d.doc) for d in self.docs.values()],
                []
            )

    def get_term_count(self, filter_func=filter_tokens, by_entry=True,
                       randomized=True):
        if by_entry:
            data = self.get_all_doc_entries(randomized)
        else:
            data = [d.doc for d in self.docs.values()]
        cv = CountVectorizer(
            data,
            analyzer=lambda x: [t.lemma_ for t in filter_func(x)]
        )
        return (cv, data)

    def get_tfidf(self, filter_func=filter_tokens, by_entry=True,
                  randomized=True, max_df=0.95,
                  min_df=2):
        if by_entry:
            data = self.get_all_doc_entries(randomized)
        else:
            data = [d.doc for d in self.docs.values()]
        tv = TfidfVectorizer(
            data,
            analyzer=lambda x: [t.lemma_ for t in filter_func(x)],
            max_df=max_df,
            min_df=min_df
        )
        return (tv, data)


class WeightedModel:
    def __init__(self, nlp):
        self.nlp = nlp
        self.documents = []
        self.tfidf = None
