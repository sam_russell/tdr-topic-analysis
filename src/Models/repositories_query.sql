CREATE VIEW IF NOT EXISTS scores_diff
AS
SELECT 
		assessment.id,
		assessment.repository_id,
		repository.name,
		guideline_package.name,
		assessment.guideline_package_id,
		SUM(assessment_entry.compliance_level) AS cumulative_score,
		COUNT(guideline_package.id) AS sections_count
	FROM assessment
	INNER JOIN assessment_entry
	ON assessment_entry.assessment_id=assessment.id
	INNER JOIN repository
	ON repository.id=assessment.repository_id
	INNER JOIN guideline_package
	ON guideline_package.id=assessment.guideline_package_id
	INNER JOIN guideline
	ON guideline.id=assessment_entry.guideline_id
	WHERE
		NOT guideline.id=51
		AND NOT guideline.id=34
		AND NOT guideline.id=33
	GROUP BY assessment.id;

SELECT 
	scores_diff.repository_id, 
	scores_diff.name , 
	dsa2010.cumulative_score AS DSA_2010_score,
	dsa2014.cumulative_score AS DSA_2014_score,
	cts2017.cumulative_score AS CTS_2017_score
FROM scores_diff
LEFT JOIN (SELECT 
				scores_diff.repository_id, 
				scores_diff.cumulative_score
			FROM scores_diff 
			WHERE scores_diff.guideline_package_id=1) 
			AS dsa2010
ON dsa2010.repository_id=scores_diff.repository_id
LEFT JOIN (SELECT
			scores_diff.repository_id, 
			scores_diff.cumulative_score
			FROM scores_diff 
			WHERE scores_diff.guideline_package_id=2)
			AS dsa2014
ON dsa2014.repository_id=scores_diff.repository_id
LEFT JOIN (SELECT
			scores_diff.repository_id,
			scores_diff.cumulative_score
			FROM scores_diff
			WHERE scores_diff.guideline_package_id=3)
			AS cts2017
ON cts2017.repository_id=scores_diff.repository_id
GROUP BY scores_diff.repository_id;
