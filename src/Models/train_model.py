from functools import reduce, partial
import math
from typing import Dict, List

from bs4 import BeautifulSoup
import gensim.models
import gensim.utils
import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lookups import Lookups

from src.Data.database_migration import SQLiteManager
from src.Data.text_models import AssessmentEntry, Assessment, Repository, \
    Guideline, GuidelinePackage
DB = "data/interim/data_repository_assmts_2020-10-16T09:26:05.db"
#DB = "data/interim/data_repository_assmts_2020-10-23T12:40:32.db"
REPO_QUERY = "src/Models/repositories_query.sql"

NLP = spacy.load("en_vectors_web_lg")


def load_queries(filepath):
    with open(filepath, "r") as f:
        queries = f.read()
        queries = [i for i in queries.split(";") if i != '\n']
        return [i + ";\n" for i in queries]


def prune_tokens(nlp, doc):
    tokens = [token.lemma_ for token in doc]
    pruned = [
        t for t in tokens
        if not nlp.vocab[t].is_stop
        and not nlp.vocab[t].is_space
        and not nlp.vocab[t].is_punct
        and not nlp.vocab[t].is_digit
        and not nlp.vocab[t].like_url
    ]
    clean_doc = spacy.tokens.Doc(nlp.vocab, words=pruned)
    return clean_doc


class AssessmentDocument:
    """Contains a spacy.tokens.doc.Doc instance, with contextual metadata"""

    def __init__(self,
                 nlp_inst: spacy.lang.en.English,
                 assmt_entry: AssessmentEntry,
                 **pipes):
        self.nlp = nlp_inst
        self.assmt_entry = assmt_entry
        self.doc = None
        for pipe_name, pipe_callable in pipes.items():
            if not self.nlp.has_pipe(pipe_name):
                component = partial(pipe_callable, nlp_inst)
                self.nlp.add_pipe(component, name=pipe_name)

    def preprocess_text(self) -> List[str]:
        """Takes models.Entry obj or string, lemmatizes and tokenizes"""
        tokens = [token.lemma_ for token in self.doc]
        return [
            t.lower() for t in tokens
            if not self.nlp.vocab[t].is_stop
            and not self.nlp.vocab[t].is_space
            and not self.nlp.vocab[t].is_punct
            and not self.nlp.vocab[t].is_digit
        ]

    @classmethod
    def make(cls,
             nlp_inst: spacy.lang.en.English,
             assmt_entry: AssessmentEntry):
        inst = cls(nlp_inst, assmt_entry, prune_tokens=prune_tokens)
        try:
            inst.doc = inst.nlp(assmt_entry)
        except (AttributeError, TypeError):
            inst.doc = inst.nlp(assmt_entry.response)
        return inst


class TextCleaner:
    """Removes markup, stopwords, and bric-a-brac from texts"""

    def __init__(self, db_filepath):
        self.session = SQLiteManager.make(db_filepath).session
        self.docs = []

    def entries(self):
        entries = self.session.query(AssessmentEntry).all()
        return [i for i in entries if i.response]

    def entries_per_assessment(self):
        entries = self.entries()
        assmt_ids = [entry.assessment_id for entry in entries]
        return {
            i: "\n".join(
                [e.response for e in entries if e.assessment_id == i]
                )
            for i in assmt_ids
        }

    def preprocess_assessments(self):
        self.docs = {
            key: AssessmentDocument.make(
                NLP,
                BeautifulSoup(entry).get_text()
            )
            for key, entry in self.entries_per_assessment().items()
        }
        return self.docs
        # return [doc.preprocess_text() for doc in self.docs.values()]

    def preprocess_entries(self):
        self.docs = [
            AssessmentDocument.make(NLP, entry)
            for entry in self.entries()
        ]
        return [doc.preprocess_text() for doc in self.docs]
        # preprocessed = [self.preprocess_text(e) for e in self.entries()]
        # return preprocessed

    def preprocess_text_(self, entry):
        """deprecated"""
        try:
            tokens = gensim.utils.simple_preprocess(entry.response)
        except AttributeError:
            tokens = gensim.utils.simple_preprocess(entry)
        return [t for t in tokens if not NLP.vocab[t].is_stop]

    def preprocess_entries_(self):
        """deprecated"""
        preprocessed = [
            [
                t for t in
                gensim.utils.simple_preprocess(entry.response)
                if not NLP.vocab[t].is_stop
            ]
            for entry in self.entries()
        ]
        return preprocessed


def assmts_comparison_lookup(filepath=REPO_QUERY, tc: TextCleaner = None):
    if not tc:
        tc = TextCleaner(DB)
    q_view, q_scores, *rest = load_queries(filepath)
    tc.session.execute(q_view)
    tc.session.commit()
    repos = tc.session.execute(q_scores).fetchall()
    comparisons = {
        "2010_2014": [
            i for i in repos
            if i.DSA_2010_score is not None
            and i.DSA_2014_score is not None
        ],
        "2014_2017": [
            i for i in repos
            if i.DSA_2014_score is not None
            and i.CTS_2017_score is not None
        ],
        "2010_2017": [
            i for i in repos
            if i.DSA_2010_score is not None
            and i.CTS_2017_score is not None
        ]
    }
    return comparisons


def assmts_comparison_texts(filepath=REPO_QUERY, tc: TextCleaner = None):
    if not tc:
        tc = TextCleaner(DB)
    comparisons = assmts_comparison_lookup(filepath, tc)
    scores_view = tc.session.execute(
        "SELECT * FROM scores_diff;").fetchall()
    assmts = {
        "2010_2014": [
            tc.session.query(
                Assessment
            ).filter(
                (Assessment.guideline_package_id == 1)
                | (Assessment.guideline_package_id == 2)
            ).filter_by(repository_id=repo.repository_id).all()
            for repo in comparisons["2010_2014"]
        ],
        "2014_2017": [
            tc.session.query(
                Assessment
            ).filter(
                (Assessment.guideline_package_id == 2)
                | (Assessment.guideline_package_id == 3)
            ).filter_by(repository_id=repo.repository_id).all()
            for repo in comparisons["2014_2017"]
        ],
        "2010_2017": [
            tc.session.query(
                Assessment
            ).filter(
                (Assessment.guideline_package_id == 1)
                | (Assessment.guideline_package_id == 3)
            ).filter_by(repository_id=repo.repository_id).all()
            for repo in comparisons["2010_2017"]
        ]
    }
    return {
        key: [
            [
                {
                    "repository": assmt.repository.name,
                    "guideline_package": assmt.guideline_package.name,
                    "responses": [
                        entry.response for entry in
                        tc.session.query(
                            AssessmentEntry
                        ).filter_by(assessment_id=assmt.id).all()
                    ]
                }
                for assmt in repo]
            for repo in val
        ]
        for key, val in assmts.items()
    }


class ModelBuilder:

    def __init__(self):
        pass

    def make_word2vec(self, db_filepath, **kwargs):
        tc = TextCleaner(db_filepath)
        model = gensim.models.Word2Vec(
            tc.preprocess_entries(), **kwargs)
        return model

    def make_dictionary(self, db_filepath, **kwargs):
        tc = TextCleaner(db_filepath)
        return gensim.corpora.Dictionary(tc.preprocess_entries())

    def make_bow_corpus(self, dictionary, corpus, **kwargs):
        return [dictionary.doc2bow(text) for text in corpus]

    def make_lsi_model(self, dictionary, bow_corpus, **kwargs):
        lsi = gensim.models.LsiModel(
            bow_corpus, id2word=dictionary, **kwargs)
        return lsi

    def make_lda(self, db_filepath, **kwargs):
        tc = TextCleaner(db_filepath)
        pass

    def make_doc2vec_corpus(self, db_filepath):
        tc = TextCleaner(db_filepath)
        corpus = [
            gensim.models.doc2vec.TaggedDocument(
                tokens,
                [idx]
                )
            for idx, tokens
            in enumerate(tc.preprocess_entries())
            ]
        return corpus

    def make_doc2vec_model(self, corpus):
        model = gensim.models.doc2vec.Doc2Vec(
            vector_size=200,
            min_count=2,
            epochs=50
            )
        model.build_vocab(corpus)
        model.train(
            corpus,
            total_examples=model.corpus_count,
            epochs=model.epochs
        )
        return model


# def test_query(session, repository_id: int):
#     repo = session.query(
#         Assessment
#     ).filter_by(repository_id=repository_id).all()
#     if repo:
#         dsa_2010 = [i for i in repo if i.guideline_package_id == 1]
#         dsa_2014 = [i for i in repo if i.guideline_package_id == 2]
#         cts_2017 = [i for i in repo if i.guideline_package_id == 3]
#     else:
#         return [None, None, None]
#     assmts = []
#     for assmt in [dsa_2010, dsa_2014, cts_2017]:
#         if len(assmt) == 0:
#             assmts.append(None)
#         else:
#             assmts.append(session.query(
#                 AssessmentEntry
#             ).filter_by(assessment_id=assmt.id).all()
#     return assmts
# 

# def test_icpsr_query(session):
#     icpsr = session.query(
#         Assessment
#     ).filter_by(repository_id=2).all()
#     dsa_2010, *rest = [i for i in icpsr if i.guideline_package_id == 1]
#     dsa_2014, = [i for i in icpsr if i.guideline_package_id == 2]
#     cts_2017, = [i for i in icpsr if i.guideline_package_id == 3]
#     assmts = {
#         "2010": session.query(
#             AssessmentEntry
#         ).filter_by(assessment_id=dsa_2010.id).all(),
#         "2014": session.query(
#             AssessmentEntry
#         ).filter_by(assessment_id=dsa_2014.id).all(),
#         "2017": session.query(
#             AssessmentEntry
#         ).filter_by(assessment_id=cts_2017.id).all()
#     }
#     return assmts


def concat_responses(responses, tc: TextCleaner = None):
    if not tc:
        tc = TextCleaner(DB)
    return reduce(
        lambda x, y: x + y,
        [
            tc.preprocess_text(entry_text) for entry_text
            in responses
        ]
    )


def test_icpsr_preprocess(tc, assmts):
    def reduce_text(key):
        return reduce(
            lambda x, y: x + y,
            [tc.preprocess_text(t) for t in assmts[key]],
            []
        )
    preprocessed = {
        "2010": reduce_text("2010"),
        "2014": reduce_text("2014"),
        "2017": reduce_text("2017")
        }
    return preprocessed


class Comparison:

    def __init__(self,
                 doc_a=None,
                 doc_b=None,
                 assmt_a=None,
                 assmt_b=None,
                 similarity=None,
                 terms_difference=None,
                 terms_added=None,
                 terms_removed=None,
                 vector_a=None,
                 vector_norm_a=None,
                 vector_b=None,
                 vector_norm_b=None,
                 repository_name=None,
                 repository_id=None,
                 guideline_package_name_a=None,
                 guideline_package_name_b=None,
                 guideline_package_id_a=None,
                 guideline_package_id_b=None,
                 *args, **kwargs):
        """Not meant to be called directly, use `make` factory method """
        self._doc_a = doc_a
        self._doc_b = doc_b
        self._assmt_a = assmt_a
        self._assmt_b = assmt_b
        self.similarity = similarity
        self.terms_difference = terms_difference
        self.terms_added = terms_added
        self.terms_removed = terms_removed
        self.vector_a = vector_a
        self.vector_norm_a = vector_norm_a
        self.vector_b = vector_b
        self.vector_norm_b = vector_norm_b
        self.repository_name = repository_name
        self.repository_id = repository_id
        self.guideline_package_name_a = guideline_package_name_a
        self.guideline_package_name_b = guideline_package_name_b
        self.guideline_package_id_a = guideline_package_id_a
        self.guideline_package_id_b = guideline_package_id_b
        self.comparison_name = "/".join([guideline_package_name_a,
                                         guideline_package_name_b])

    @staticmethod
    def L2_norm(vector):
        """Computes vector magnitude"""
        return math.sqrt(reduce(lambda x, y: x + y, [i * i for i in vector]))

    @classmethod
    def compare_docs(cls, texts, a, b):
        similarity = texts[a.id].doc.similarity(texts[b.id].doc)
        a_terms = set([i.string for i in texts[a.id].doc])
        b_terms = set([i.string for i in texts[b.id].doc])
        vector_a = texts[a.id].doc.vector
        vector_b = texts[b.id].doc.vector
        vector_norm_a = cls.L2_norm(vector_a)
        vector_norm_b = cls.L2_norm(vector_b)
        difference = a_terms.difference(b_terms)
        terms_added = [i for i in difference if i not in a_terms]
        terms_removed = [i for i in difference if i not in b_terms]
        return {
            "similarity": similarity,
            "terms_difference": difference,
            "terms_added": terms_added,
            "terms_removed": terms_removed,
            "vector_a": vector_a,
            "vector_b": vector_b,
            "vector_norm_a": vector_norm_a,
            "vector_norm_b": vector_norm_b
        }

    @classmethod
    def make(cls, session, texts, repo_id, pkg_id_a, pkg_id_b):
        assmts = session.query(Assessment).filter_by(repository_id=repo_id).all()
        (a, b, *rest) = set(
            [i for i in assmts if i.guideline_package_id == pkg_id_a]
        ).union(
            set([i for i in assmts if i.guideline_package_id == pkg_id_b])
        )
        # there is only one case, ICPSR 2010 DSA assessment, where
        # multiple assessments exist for a single repository, we
        # choose to ignore the earlier, "pilot" version, at this time.
        if not b:
            raise ValueError("No assessment found for pkg_id_b")
        if rest:
            a = b
            b = rest[0]
        sims = cls.compare_docs(texts, a, b)
        inst = cls(
            assmt_a=a,
            assmt_b=b,
            doc_a=texts[a.id].doc,
            doc_b=texts[b.id].doc,
            repository_name=a.repository.name,
            repository_id=repo_id,
            guideline_package_name_a=a.guideline_package.name,
            guideline_package_name_b=b.guideline_package.name,
            guideline_package_id_a=a.guideline_package_id,
            guideline_package_id_b=b.guideline_package_id,
            **sims
        )
        return inst

    @classmethod
    def make_set(cls, session, texts, pkg_id_a, pkg_id_b):
        comparisons = []
        for repo_id in set(
                [
                    i.repository_id for i in session.query(Assessment).all()
                ]
        ):
            try:
                c = cls.make(tc.session, texts, repo_id, pkg_id_a, pkg_id_b)
                comparisons.append(c)
            except ValueError as e:
                print(e)
                comparisons.append(0)
        return comparisons

    @classmethod
    def make_all(cls, session, texts):
        dsa2010_dsa2014 = cls.make_set(session, texts, 1, 2)
        dsa2014_cts2017 = cls.make_set(session, texts, 2, 3)
        dsa2010_cts2017 = cls.make_set(session, texts, 1, 3)
        return [dsa2010_dsa2014, dsa2014_cts2017, dsa2010_cts2017]


def L2_norm(vector):
    return math.sqrt(reduce(lambda x, y: x + y, [i * i for i in vector]))


def compare_assmts(session, texts, repo_id, pkg_id_a, pkg_id_b):
    assmts = session.query(Assessment).filter_by(repository_id=repo_id).all()
    (a, b, *rest) = set(
        [i for i in assmts if i.guideline_package_id == pkg_id_a]
    ).union(
        set([i for i in assmts if i.guideline_package_id == pkg_id_b])
    )
    # there is only one case, ICPSR 2010 DSA assessment, where
    # multiple assessments exist for a single repository, we
    # choose to ignore the earlier, "pilot" version, at this time.
    if rest:
        a = b
        b = rest[0]
    similarity = texts[a.id].doc.similarity(texts[b.id].doc)
    a_terms = set([i.string for i in texts[a.id].doc])
    b_terms = set([i.string for i in texts[b.id].doc])
    difference = a_terms.difference(b_terms)
    terms_added = [i for i in difference if i not in a_terms]
    terms_removed = [i for i in difference if i not in b_terms]


tc = TextCleaner(DB)
#texts = tc.preprocess_entries()
texts = tc.preprocess_assessments()

# dsa2010_dsa2014 = []
# for repo_id in set([i.repository_id for i in tc.session.query(Assessment).all()]):
#     try:
#         c = Comparison.make(tc.session, texts, repo_id, 1, 2)
#         comparisons.append(c)
#     except ValueError as e:
#         print(e)
#         comparisons.append(0)
#


doc_data = Comparison.make_all(tc.session, texts)


# d = gensim.corpora.Dictionary(texts)
# bow_corpus = [d.doc2bow(t) for t in texts]
# index = gensim.similarities.MatrixSimilarity(bow_corpus, num_features=len(d))

    # sorted([i for i in enumerate(index[bow_corpus[0]])][:10], key=lambda x: x[1], reverse=True)

# d2v.wv.wmdistance(data["2010"], data["2014"])
