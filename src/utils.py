import csv
from datetime import datetime
import io
import logging
import os
import re
import requests
import zipfile
from typing import List, Dict


def load_envfile(envfile_path: str = ".env"):
    """
A quick and dirty env var loader
because I don't want to mess with
any other packages and deps!!
"""
    with open(envfile_path) as f:
        lines = f.read().split("\n")
        not_commented = [
            ln for ln in lines
            if not re.match(r"^.*#.*$", ln)
            and not re.match(r"^.*(\s)+.*$", ln)
            and re.match(r"^.+=.+$", ln)
        ]
        env_vars = [
            tuple(var.split("=")) for var
            in not_commented
            if var
        ]
        for k, v in env_vars:
            os.environ[k] = v


def timestamped(str_data, timestamp=None):
    if not timestamp:
        timestamp = datetime.now()
    timestamp = timestamp.strftime('%FT%T')
    return str_data + f"_{timestamp}"


def dir_or_create(dir_path: str) -> str:
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
    return dir_path


def remove_trailing_ws(s):
    if s[-1] == " ":
        return remove_trailing_ws(s[:-1])
    else:
        return s


def comma_sep_string(str_list):
    return ",".join([i for i in str_list])


def read_csv(csv_path):
    with open(csv_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            yield row


def nltk_setup(
        nltk_data_packages: Dict = {
            "misc": [],
            "sentiment": [],
            "taggers": [],
            "corpora": ["https://raw.githubusercontent.com/nltk/nltk_data/gh-pages/packages/corpora/stopwords.zip"],
            "help": [],
            "models": [],
            "stemmers": [],
            "tokenizers": ["https://raw.githubusercontent.com/nltk/nltk_data/gh-pages/packages/tokenizers/punkt.zip"]
        },
        envfile_path: str = None):
    """
Creates directory structure for NLTK data files 
and populates it with the specified datasets and 
models provided by "NLTK.org". 
"""
    load_envfile()
    nltk_data_path = os.environ.get("NLTK_DATA")
    if not os.path.exists(nltk_data_path):
        os.mkdir(nltk_data_path)
        for k, v in nltk_data_packages.items():
            subdir = os.path.join(nltk_data_path, k)
            os.mkdir(subdir)
            for url in v:
                download = requests.get(url)
                if download:
                    with io.BytesIO(download.content) as buf:
                        zipfile.ZipFile(buf).extractall(subdir)
                else:
                    print("SCRIPT BUNGLED: bad dl")
                    # TODO: actually log this properly
    else:
        return False


def month_num(month: str) -> str:
    months = {
        "January": "01",
        "February": "02",
        "March": "03",
        "April": "04",
        "May": "05",
        "June": "06",
        "July": "07",
        "August": "08",
        "September": "09",
        "October": "10",
        "November": "11",
        "December": "12"
    }
    return months[month]


if __name__ == "__main__":
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    pass
