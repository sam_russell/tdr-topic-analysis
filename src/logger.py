from datetime import datetime
import logging
import os

from logging import FileHandler
from logging import Formatter


# logs:
#  1.  Data download log
#      - what files are downloaded?
#      - to what path are they saved?
#  2.  DSA MySQL processing log
#    - what entities are retrieved for inclusion in dataset?
#    - which entities are "stubs" (ie. there is a repository, and
#      an assessment, but no actual assessment entries?
#    - which entities are "test" or dummy entities?
#    - which assessments have a seal date?  status code (bool)?
#  3.  PDF extraction log
#    - which ones fail?
#    - which ones succeed?
#    - which files are matched with a repository in the DSA MySQL data?
#    - which files are not matched?
#    - for all the failures, is there redundancy provided by data
#      from the DSA MySQL db?
#  4.  Tasks log
#    - record actual human tasks that must be done.

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

LOG_LEVEL = logging.INFO


class ProjectLogger:
    """Loggers for recording information about provenance & quality"""

    def __init__(self,
                 logger_name: str = None,
                 file_handler: FileHandler = None,
                 process_timestamp: datetime = None):
        self.logger = logging.getLogger(logger_name)
        self._file_handler = file_handler
        self.process_timestamp = process_timestamp

    @staticmethod
    def create_dir(dir_path: str):
        base_base = os.path.split(dir_path)[0]
        if not os.path.exists(base_base):
            os.mkdir(base_base)
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)

    @classmethod
    def make(cls,
             logger_name: str = None,
             log_dir_base: str = None,
             log_file_base: str = None,
             log_level: int = logging.INFO,
             process_timestamp: datetime = None,
             log_format: str = LOG_FORMAT,
             **kwargs):
        log_file = log_file_base + "_{}".format(
            process_timestamp.strftime('%FT%T')) + ".log"
        log_dir = log_dir_base + "_{}".format(
            process_timestamp.strftime('%FT%T'))
        cls.create_dir(log_dir)
        log_file = os.path.join(log_dir, log_file)
        file_handler = FileHandler(log_file)
        file_handler.setLevel(log_level)
        file_handler.setFormatter(Formatter(log_format))
        inst = cls(logger_name, file_handler, process_timestamp)
        # mutating logger state
        inst.logger.setLevel(log_level)
        inst.logger.addHandler(inst._file_handler)
        return inst


class Config:
    """Configuration keyword args for ProjectLogger"""
    LOG_DIR_BASE = os.path.join("logs", "logs")

    DATA_ACQUISITION = {
        "logger_name": "project_logger.acquisition",
        "log_dir_base": LOG_DIR_BASE,
        "log_file_base": "acquisition"
    }

    DSA_MYSQL = {
        "logger_name": "project_logger.dsa_mysql",
        "log_dir_base": LOG_DIR_BASE,
        "log_file_base": "dsa_mysql"
    }

    PDF_INTEGRATION = {
        "logger_name": "project_logger.pdf_integration",
        "log_dir_base": LOG_DIR_BASE,
        "log_file_base": "pdf_integration"
    }

    TASKS = {
        "logger_name": "project_logger.tasks",
        "log_dir_base": LOG_DIR_BASE,
        "log_file_base": "tasks"
    }

# data acquisition logger


# DSA MySQL logger


# PDF integration logger


# tasks logger

# messaging logger
